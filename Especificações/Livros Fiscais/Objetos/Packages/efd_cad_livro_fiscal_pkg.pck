create or replace package efd_cad_livro_fiscal_pkg is

procedure INSERIR( p_id            efd_cad_livro_fiscal_tab.id%TYPE
                 , p_modelo        efd_cad_livro_fiscal_tab.modelo%TYPE
                 , p_id_empresa    efd_cad_livro_fiscal_tab.id_empresa%TYPE
                 , p_firma         efd_cad_livro_fiscal_tab.firma%TYPE
                 , p_cnpj_cpf      efd_cad_livro_fiscal_tab.cnpj_cpf%TYPE
                 , p_insc_estadual efd_cad_livro_fiscal_tab.insc_estadual%TYPE
                 , p_data_inicio   efd_cad_livro_fiscal_tab.data_inicio%TYPE
                 , p_data_fim      efd_cad_livro_fiscal_tab.data_fim%TYPE );
                 
procedure ALTERAR( p_id            efd_cad_livro_fiscal_tab.id%TYPE
                 , p_modelo        efd_cad_livro_fiscal_tab.modelo%TYPE
                 , p_id_empresa    efd_cad_livro_fiscal_tab.id_empresa%TYPE
                 , p_firma         efd_cad_livro_fiscal_tab.firma%TYPE
                 , p_cnpj_cpf      efd_cad_livro_fiscal_tab.cnpj_cpf%TYPE
                 , p_insc_estadual efd_cad_livro_fiscal_tab.insc_estadual%TYPE
                 , p_data_fim      efd_cad_livro_fiscal_tab.data_fim%TYPE );
                 
procedure REMOVER( p_id efd_cad_livro_fiscal_tab.id%TYPE );                 

end efd_cad_livro_fiscal_pkg;
/
create or replace package body efd_cad_livro_fiscal_pkg is

v_sql_dynamic clob;


PROCEDURE INSERIR( p_id            efd_cad_livro_fiscal_tab.id%TYPE
                 , p_modelo        efd_cad_livro_fiscal_tab.modelo%TYPE
                 , p_id_empresa    efd_cad_livro_fiscal_tab.id_empresa%TYPE
                 , p_firma         efd_cad_livro_fiscal_tab.firma%TYPE
                 , p_cnpj_cpf      efd_cad_livro_fiscal_tab.cnpj_cpf%TYPE
                 , p_insc_estadual efd_cad_livro_fiscal_tab.insc_estadual%TYPE
                 , p_data_inicio   efd_cad_livro_fiscal_tab.data_inicio%TYPE
                 , p_data_fim      efd_cad_livro_fiscal_tab.data_fim%TYPE ) IS


       v_id            efd_cad_livro_fiscal_tab.id%TYPE;
       v_modelo        efd_cad_livro_fiscal_tab.modelo%TYPE;
       v_id_empresa    efd_cad_livro_fiscal_tab.id_empresa%TYPE;
       v_firma         efd_cad_livro_fiscal_tab.firma%TYPE;
       v_cnpj_cpf      efd_cad_livro_fiscal_tab.cnpj_cpf%TYPE;
       v_insc_estadual efd_cad_livro_fiscal_tab.insc_estadual%TYPE;
       v_data_inicio   efd_cad_livro_fiscal_tab.data_inicio%TYPE;
       v_data_fim      efd_cad_livro_fiscal_tab.data_fim%TYPE;
       
       BEGIN
         
         v_id            := p_id;
         v_modelo        := p_modelo;
         v_id_empresa    := p_id_empresa;
         v_firma         := p_firma;
         v_cnpj_cpf      := p_cnpj_cpf;
         v_insc_estadual := p_insc_estadual;
         v_data_inicio   := p_data_inicio;
         v_data_fim      := p_data_fim;
         
         v_sql_dynamic := 'INSERT INTO EFD_CAD_LIVRO_FISCAL_TAB ( ID
                                                                , MODELO
                                                                , ID_EMPRESA
                                                                , FIRMA
                                                                , CNPJ_CPF
                                                                , INSC_ESTADUAL
                                                                , DATA_INICIO
                                                                , DATA_FIM
                                                                , DATA_GERACAO
                                                                , XML )
                                                         VALUES ( :id
                                                                , :modelo
                                                                , :id_empresa
                                                                , :firma
                                                                , :cnpj_cpf
                                                                , :insc_estadual
                                                                , :data_inicio
                                                                , :data_fim
                                                                , sysdate )';
                                                                
         
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_id
             , v_modelo
             , v_id_empresa
             , v_firma
             , v_cnpj_cpf
             , v_insc_estadual
             , v_data_inicio
             , v_data_fim;             
         COMMIT;
         
         EXCEPTION
           WHEN OTHERS THEN
             ROLLBACK;
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'efd_cad_livro_fiscal_pkg.inserir');
             dbms_output.put_line(SQLERRM);  
         

END INSERIR; 


PROCEDURE ALTERAR( p_id            efd_cad_livro_fiscal_tab.id%TYPE
                 , p_modelo        efd_cad_livro_fiscal_tab.modelo%TYPE
                 , p_id_empresa    efd_cad_livro_fiscal_tab.id_empresa%TYPE
                 , p_firma         efd_cad_livro_fiscal_tab.firma%TYPE
                 , p_cnpj_cpf      efd_cad_livro_fiscal_tab.cnpj_cpf%TYPE
                 , p_insc_estadual efd_cad_livro_fiscal_tab.insc_estadual%TYPE
                 , p_data_fim      efd_cad_livro_fiscal_tab.data_fim%TYPE ) IS
                 
       v_id            efd_cad_livro_fiscal_tab.id%TYPE;
       v_modelo        efd_cad_livro_fiscal_tab.modelo%TYPE;
       v_id_empresa    efd_cad_livro_fiscal_tab.id_empresa%TYPE;
       v_firma         efd_cad_livro_fiscal_tab.firma%TYPE;
       v_cnpj_cpf      efd_cad_livro_fiscal_tab.cnpj_cpf%TYPE;
       v_insc_estadual efd_cad_livro_fiscal_tab.insc_estadual%TYPE;
       v_data_fim      efd_cad_livro_fiscal_tab.data_fim%TYPE;
       
       BEGIN
         
         v_id            := p_id;
         v_modelo        := p_modelo;
         v_id_empresa    := p_id_empresa;
         v_firma         := p_firma;
         v_cnpj_cpf      := p_cnpj_cpf;
         v_insc_estadual := p_insc_estadual;
         v_data_fim      := p_data_fim;
         
         v_sql_dynamic := 'UPDATE EFD_CAD_LIVRO_FISCAL_TAB
                              SET MODELO        = :modelo
                                , ID_EMPRESA    = :id_empresa
                                , FIRMA         = :firma
                                , CNPJ_CPF      = :cnpj_cpf
                                , INSC_ESTADUAL = :insc_estadual
                                , DATA_FIM      = :data_fim 
                            WHERE ID = :id';
           
           
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_modelo
             , v_id_empresa
             , v_firma
             , v_cnpj_cpf
             , v_insc_estadual
             , v_data_fim
             , v_id;
         
             
         COMMIT;
         
         EXCEPTION
           WHEN OTHERS THEN
             ROLLBACK;
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'efd_cad_livro_fiscal_pkg.alterar');
             dbms_output.put_line(SQLERRM);     
                     
                 
END ALTERAR;

PROCEDURE REMOVER( p_id efd_cad_livro_fiscal_tab.id%TYPE ) IS
  
       v_id efd_cad_livro_fiscal_tab.id%TYPE;
       
       BEGIN
         
         v_id := p_id;
         
         v_sql_dynamic := 'DELETE 
                             FROM EFD_CAD_LIVRO_FISCAL_TAB
                            WHERE ID = :id';
         
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_id;
         
         COMMIT;
         
         EXCEPTION
           WHEN OTHERS THEN
             ROLLBACK;
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'efd_cad_livro_fiscal_pkg.remover');
             dbms_output.put_line(SQLERRM);
  
END REMOVER;

end efd_cad_livro_fiscal_pkg;
/
