create or replace package gsf_regra_liv_fiscal_P1_pkg is
TYPE   rec_regra IS RECORD (
       cod_valor_fiscal       VARCHAR2(1) 
     , base_calculo           VARCHAR2(1)
     , aliquota               VARCHAR2(1)
     , imposto                VARCHAR2(1));

TYPE t_regra IS VARRAY(1) OF rec_regra;

procedure INSERIR( p_id_empresa               gsf_regra_liv_fiscal_p1_tab.id_empresa%TYPE
                 , p_tipo                     gsf_regra_liv_fiscal_p1_tab.tipo%TYPE
                 , p_cod_valor_fiscal         gsf_regra_liv_fiscal_p1_tab.cod_valor_fiscal%TYPE
                 , p_uf                       gsf_regra_liv_fiscal_p1_tab.uf%TYPE
                 , p_cfop                     gsf_regra_liv_fiscal_p1_tab.cfop%TYPE
                 , p_cst                      gsf_regra_liv_fiscal_p1_tab.cst%TYPE
                 , p_base_calculo             gsf_regra_liv_fiscal_p1_tab.base_calculo%TYPE
                 , p_aliquota                 gsf_regra_liv_fiscal_p1_tab.aliquota%TYPE
                 , p_imposto                  gsf_regra_liv_fiscal_p1_tab.imposto%TYPE);
                 

procedure ALTERAR( p_id_regra                 gsf_regra_liv_fiscal_p1_tab.id_regra%TYPE
                 , p_id_empresa               gsf_regra_liv_fiscal_p1_tab.id_empresa%TYPE
                 , p_tipo                     gsf_regra_liv_fiscal_p1_tab.tipo%TYPE
                 , p_cod_valor_fiscal         gsf_regra_liv_fiscal_p1_tab.cod_valor_fiscal%TYPE
                 , p_uf                       gsf_regra_liv_fiscal_p1_tab.uf%TYPE
                 , p_cfop                     gsf_regra_liv_fiscal_p1_tab.cfop%TYPE
                 , p_cst                      gsf_regra_liv_fiscal_p1_tab.cst%TYPE
                 , p_base_calculo             gsf_regra_liv_fiscal_p1_tab.base_calculo%TYPE
                 , p_aliquota                 gsf_regra_liv_fiscal_p1_tab.aliquota%TYPE
                 , p_imposto                  gsf_regra_liv_fiscal_p1_tab.imposto%TYPE);                 
                 

procedure REMOVER( p_id_regra gsf_regra_liv_fiscal_p1_tab.id_regra%TYPE );

FUNCTION Obtem_Id_Regra (p_id_empresa NUMBER,
                         p_tipo       VARCHAR2,
                         p_uf         VARCHAR2, 
                         p_cfop       VARCHAR2, 
                         p_cst        VARCHAR2) RETURN NUMBER;

FUNCTION Obtem_Dados_Regra (p_id_empresa NUMBER,
                            p_tipo       VARCHAR2,
                            p_uf         VARCHAR2, 
                            p_cfop       VARCHAR2,
                            p_cst        VARCHAR2) RETURN t_regra;

end;
/
create or replace package body gsf_regra_liv_fiscal_P1_pkg is


v_sql_dynamic clob;


PROCEDURE INSERIR( p_id_empresa               gsf_regra_liv_fiscal_p1_tab.id_empresa%TYPE
                 , p_tipo                     gsf_regra_liv_fiscal_p1_tab.tipo%TYPE
                 , p_cod_valor_fiscal         gsf_regra_liv_fiscal_p1_tab.cod_valor_fiscal%TYPE
                 , p_uf                       gsf_regra_liv_fiscal_p1_tab.uf%TYPE
                 , p_cfop                     gsf_regra_liv_fiscal_p1_tab.cfop%TYPE
                 , p_cst                      gsf_regra_liv_fiscal_p1_tab.cst%TYPE
                 , p_base_calculo             gsf_regra_liv_fiscal_p1_tab.base_calculo%TYPE
                 , p_aliquota                 gsf_regra_liv_fiscal_p1_tab.aliquota%TYPE
                 , p_imposto                  gsf_regra_liv_fiscal_p1_tab.imposto%TYPE) IS
                 

       v_id_regra                 gsf_regra_liv_fiscal_p1_tab.id_regra%TYPE;
       v_id_empresa               gsf_regra_liv_fiscal_p1_tab.id_empresa%TYPE;
       v_tipo                     gsf_regra_liv_fiscal_p1_tab.tipo%TYPE;
       v_cod_valor_fiscal         gsf_regra_liv_fiscal_p1_tab.cod_valor_fiscal%TYPE;
       v_uf                       gsf_regra_liv_fiscal_p1_tab.uf%TYPE;
       v_cfop                     gsf_regra_liv_fiscal_p1_tab.cfop%TYPE;
       v_cst                      gsf_regra_liv_fiscal_p1_tab.cst%TYPE;
       v_base_calculo             gsf_regra_liv_fiscal_p1_tab.base_calculo%TYPE;
       v_aliquota                 gsf_regra_liv_fiscal_p1_tab.aliquota%TYPE;
       v_imposto                  gsf_regra_liv_fiscal_p1_tab.imposto%TYPE;
       
       BEGIN
         
         v_id_empresa               := p_id_empresa;
         v_tipo                     := p_tipo;
         v_cod_valor_fiscal         := p_cod_valor_fiscal;
         v_uf                       := p_uf;
         v_cfop                     := p_cfop;
         v_cst                      := p_cst;
         v_base_calculo             := p_base_calculo;
         v_aliquota                 := p_aliquota;
         v_imposto                  := p_imposto;
         
         SELECT gsf_liv_fiscal_regra_p1_seq.nextval 
           INTO v_id_regra 
           FROM dual;
         
         v_sql_dynamic := 'INSERT INTO gsf_regra_liv_fiscal_p1_tab( ID_REGRA
                                                     , ID_EMPRESA
                                                     , TIPO
                                                     , COD_VALOR_FISCAL
                                                     , UF
                                                     , CFOP
                                                     , CST
                                                     , BASE_CALCULO
                                                     , ALIQUOTA
                                                     , imposto)
                                              VALUES ( :id_regra
                                                     , :id_empresa
                                                     , :tipo
                                                     , :cod_valor_fiscal
                                                     , :uf
                                                     , :cfop
                                                     , :cst
                                                     , :base_calculo
                                                     , :aliquota
                                                     , :imposto)';
         
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_id_regra
             , v_id_empresa
             , v_tipo
             , v_cod_valor_fiscal
             , v_uf
             , v_cfop
             , v_cst
             , v_base_calculo
             , v_aliquota
             , v_imposto;
             
         COMMIT;
         
         EXCEPTION
           WHEN OTHERS THEN
             ROLLBACK;
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'REGRA_ICMS_PKG.INSERIR');
             dbms_output.put_line(SQLERRM);    
         

END INSERIR;

PROCEDURE ALTERAR( p_id_regra                 gsf_regra_liv_fiscal_p1_tab.id_regra%TYPE
                 , p_id_empresa               gsf_regra_liv_fiscal_p1_tab.id_empresa%TYPE
                 , p_tipo                     gsf_regra_liv_fiscal_p1_tab.tipo%TYPE
                 , p_cod_valor_fiscal         gsf_regra_liv_fiscal_p1_tab.cod_valor_fiscal%TYPE
                 , p_uf                       gsf_regra_liv_fiscal_p1_tab.uf%TYPE
                 , p_cfop                     gsf_regra_liv_fiscal_p1_tab.cfop%TYPE
                 , p_cst                      gsf_regra_liv_fiscal_p1_tab.cst%TYPE
                 , p_base_calculo             gsf_regra_liv_fiscal_p1_tab.base_calculo%TYPE
                 , p_aliquota                 gsf_regra_liv_fiscal_p1_tab.aliquota%TYPE
                 , p_imposto                  gsf_regra_liv_fiscal_p1_tab.imposto%TYPE) IS

       v_id_regra                 gsf_regra_liv_fiscal_p1_tab.id_regra%TYPE;
       v_tipo                     gsf_regra_liv_fiscal_p1_tab.tipo%TYPE;
       v_id_empresa               gsf_regra_liv_fiscal_p1_tab.id_empresa%TYPE;
       v_cod_valor_fiscal         gsf_regra_liv_fiscal_p1_tab.cod_valor_fiscal%TYPE;
       v_uf                       gsf_regra_liv_fiscal_p1_tab.uf%TYPE;
       v_cfop                     gsf_regra_liv_fiscal_p1_tab.cfop%TYPE;
       v_cst                      gsf_regra_liv_fiscal_p1_tab.cst%TYPE;
       v_base_calculo             gsf_regra_liv_fiscal_p1_tab.base_calculo%TYPE;
       v_aliquota                 gsf_regra_liv_fiscal_p1_tab.aliquota%TYPE;
       v_imposto                  gsf_regra_liv_fiscal_p1_tab.imposto%TYPE;
       
       BEGIN
         
         v_id_regra                 := p_id_regra;
         v_tipo                     := p_tipo;
         v_id_empresa               := p_id_empresa;
         v_cod_valor_fiscal         := p_cod_valor_fiscal;
         v_uf                       := p_uf;
         v_cfop                     := p_cfop;
         v_cst                      := p_cst;
         v_base_calculo             := p_base_calculo;
         v_aliquota                 := p_aliquota;
         v_imposto                  := p_imposto;
         
         v_sql_dynamic := 'UPDATE gsf_regra_liv_fiscal_p1_tab
                              SET ID_EMPRESA               = :id_empresa
                                , TIPO                     = :tipo
                                , COD_VALOR_FISCAL         = :cod_valor_fiscal
                                , UF                       = :uf
                                , CFOP                     = :cfop
                                , CST                      = :cst
                                , BASE_CALCULO             = :base_calculo
                                , ALIQUOTA                 = :aliquota
                                , IMPOSTO                  = :imposto
                            WHERE ID_REGRA = :id_regra';
         
         
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_id_empresa
             , v_tipo
             , v_cod_valor_fiscal
             , v_uf
             , v_cfop
             , v_cst
             , v_base_calculo
             , v_aliquota
             , v_imposto;
         
         COMMIT;
         
         EXCEPTION
           WHEN OTHERS THEN
             ROLLBACK;
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'REGRA_ICMS_PKG.ALTERAR');
             dbms_output.put_line(SQLERRM);
                      

END ALTERAR;

PROCEDURE REMOVER( p_id_regra gsf_regra_liv_fiscal_p1_tab.id_regra%TYPE ) IS
  
       v_id_regra gsf_regra_liv_fiscal_p1_tab.id_regra%TYPE;
       
       BEGIN
         
         v_id_regra := p_id_regra;
         
         v_sql_dynamic := 'DELETE 
                             FROM gsf_regra_liv_fiscal_p1_tab
                            WHERE ID_REGRA = :id_regra';
         
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_id_regra;
         
         COMMIT;
         
         EXCEPTION
           WHEN OTHERS THEN
             ROLLBACK;
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'REGRA_ICMS_PKG.REMOVER');
             dbms_output.put_line(SQLERRM);
  
END REMOVER;


FUNCTION Obtem_Id_Regra (p_id_empresa NUMBER, 
                         p_tipo       VARCHAR2,
                         p_uf         VARCHAR2, 
                         p_cfop       VARCHAR2, 
                         p_cst        VARCHAR2) RETURN NUMBER
IS
	v_temp NUMBER;
BEGIN	
  SELECT id_regra
  INTO   v_temp
  FROM	 gsf_regra_liv_fiscal_p1_tab
  WHERE  id_empresa     = p_id_empresa
  AND    tipo           = p_tipo
  AND	   NVL(uf, ' ')   = NVL(p_uf, ' ')
  AND	   NVL(cfop, ' ') = NVL(p_cfop, ' ')
  AND	   NVL(cst, ' ')  = NVL(p_cst, ' ');

	RETURN v_temp;
END;

FUNCTION Obtem_Dados_Regra (p_id_empresa NUMBER,
                            p_tipo       VARCHAR2,
                            p_uf         VARCHAR2, 
                            p_cfop       VARCHAR2,
                            p_cst        VARCHAR2) RETURN t_regra
IS	
	t_array t_regra;
BEGIN	
  SELECT cod_valor_fiscal, 
         base_calculo,
         aliquota,
         imposto
  BULK COLLECT INTO t_array
	FROM	 gsf_regra_liv_fiscal_p1_tab
  WHERE  id_empresa     = p_id_empresa
    AND	 NVL(uf, ' ')   = NVL(p_uf, ' ')
    AND	 NVL(cfop, ' ') = NVL(p_cfop, ' ')
    AND	 NVL(cst, ' ')  = NVL(p_cst, ' ');

	RETURN t_array;
END;

end;
/
