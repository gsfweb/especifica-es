﻿using System;
using System.Data;
//using System.Data.OleDb;
using Oracle.DataAccess.Client;
using System.Windows.Forms;

namespace MapWinApp
{
    public partial class FormTabelasDestino : Form
    {
        //private MultiColumnCombo.MultiColumnComboBox multiColumnComboBox;

        public FormTabelasDestino()
        {
            InitializeComponent();
        }

        private void FormTabelasDestino_Load(object sender, EventArgs e)
        {
            //this.
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (this.Tag.ToString() != "EXCLUSÃO")
            {
                if (this.comboBoxTabelasDestino.Text != null && this.textBoxDescricao.Text.Trim() != null)
                {
                    this.Cursor = Cursors.WaitCursor;

                    OracleCommand cmd1 = new OracleCommand();

                    cmd1.Connection = Database.connection;
                    cmd1.CommandText = "gsf_tabela_dest_pkg.Obtem_Descricao";
                    cmd1.CommandType = CommandType.StoredProcedure;

                    OracleParameter returnVal = new OracleParameter("RETURN_VALUE", null);
                    returnVal.Direction = ParameterDirection.ReturnValue;
                    returnVal.Size = 2000;
                    cmd1.Parameters.Add(returnVal);

                    OracleParameter p2 = new OracleParameter("p_cod_tabela_dest", this.comboBoxTabelasDestino.Text);
                    p2.Direction = ParameterDirection.Input;
                    cmd1.Parameters.Add(p2);

                    try
                    {
                        cmd1.ExecuteScalar();
                    }
                    catch (Exception ex)
                    {
                        this.Cursor = Cursors.Default;
                        System.Console.WriteLine("Exception: {0}", ex.ToString());
                        return;
                    }

                    if (returnVal.Value.ToString() == string.Empty)
                    {
                        OracleCommand cmd2 = new OracleCommand();

                        cmd2.Connection = Database.connection;
                        cmd2.CommandText = "gsf_tabela_dest_pkg.Inserir";
                        cmd2.CommandType = CommandType.StoredProcedure;

                        cmd2.Parameters.Add("p_cod_tabela_dest", this.comboBoxTabelasDestino.Text);
                        cmd2.Parameters.Add("p_ds_tabela_dest", this.textBoxDescricao.Text.Trim());

                        try
                        {
                            cmd2.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            this.Cursor = Cursors.Default;
                            System.Console.WriteLine("Exception: {0}", ex.ToString());
                            return;
                        }
                    }
                    else
                    {
                        OracleCommand cmd2 = new OracleCommand();

                        cmd2.Connection = Database.connection;
                        cmd2.CommandText = "gsf_tabela_dest_pkg.Alterar";
                        cmd2.CommandType = CommandType.StoredProcedure;

                        cmd2.Parameters.Add("p_cod_tabela_dest", this.comboBoxTabelasDestino.Text);
                        cmd2.Parameters.Add("p_ds_tabela_dest", this.textBoxDescricao.Text.Trim());

                        try
                        {
                            cmd2.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            this.Cursor = Cursors.Default;
                            System.Console.WriteLine("Exception: {0}", ex.ToString());
                            return;
                        }
                    }

                    this.Cursor = Cursors.Default;

                    this.Close();
                }
            }
            else
            {
                this.Cursor = Cursors.WaitCursor;

                OracleCommand cmd = new OracleCommand();

                cmd.Connection = Database.connection;
                cmd.CommandText = "gsf_tabela_dest_pkg.Remover";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("p_cod_tabela_dest", this.comboBoxTabelasDestino.Text);

                OracleParameter pLog = new OracleParameter("p_id_log", null);
                pLog.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(pLog);

                try
                {
                    cmd.ExecuteNonQuery();

                    try
                    {
                        if (cmd.Parameters["p_id_log"].Value.ToString() != string.Empty)
                        {
                            this.Cursor = Cursors.Default;
                            Util.Trata_Retorno(cmd.Parameters["p_id_log"].Value.ToString());
                        }
                        else
                        {
                            this.Cursor = Cursors.Default;
                            this.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        this.Cursor = Cursors.Default;
                        MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                catch (OracleException ex)
                {
                    this.Cursor = Cursors.Default;
                    MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
        }

        private void dataGridViewTabelasDestino_CellContentClick(object sender, DataGridViewCellEventArgs e)

        {

        }

        private void listViewTabelasDestino_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBoxEmpresa_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        public void comboBoxTabelasDestino_DropDown(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            this.comboBoxTabelasDestino.Items.Clear();

            OracleCommand cmd = new OracleCommand();

            cmd.Connection = Database.connection;
            cmd.CommandText = "SELECT table_name FROM sys.user_tables ORDER BY 1";
            cmd.CommandType = CommandType.Text;

            try
            {
                OracleDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    this.comboBoxTabelasDestino.Items.Add(dr[0].ToString().PadRight(dr[0].ToString().Length));
                }
            }
            catch (OracleException ex)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            this.Cursor = Cursors.Default;

        }

        private void FormTabelasDestino_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }

        }

        private void comboBoxTabelasDestino_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBoxTabelasDestino_SelectedValueChanged(object sender, EventArgs e)
        {
            if (this.comboBoxTabelasDestino.Text != string.Empty && this.textBoxDescricao.Text.Trim() != string.Empty)
            {
                this.buttonOK.Enabled = true;
            }
            else
            {
                this.buttonOK.Enabled = false;
            }
        }
    }
}

