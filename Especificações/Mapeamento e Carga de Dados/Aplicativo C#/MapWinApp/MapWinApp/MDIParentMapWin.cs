﻿using System;
using System.Data;
//using System.Data.OleDb;
using Oracle.DataAccess.Client;
using System.Windows.Forms;

namespace MapWinApp
{
    public partial class MDIParentMapWin : Form
    {
        private FormTabelasOrigem frmO = new FormTabelasOrigem();
        private FormTabelasDestino frmD = new FormTabelasDestino();
        private FormMapeamento frmM = new FormMapeamento();
        private FormTarefas frmT = new FormTarefas();

        private int sortColumn = -1;

        public MDIParentMapWin()
        {

            InitializeComponent();

        }

        private void Novo(object sender, EventArgs e)
        {
            if (this.treeViewNavegador.SelectedNode.Text.Contains("Tabelas de Origem"))
            {
                frmO.comboBoxDBLink.Enabled = true;

                frmO.comboBoxDBLink.DropDownStyle = ComboBoxStyle.DropDownList;
                frmO.comboBoxDBLink.Items.Clear();
                frmO.comboBoxDBLink.Text = string.Empty;
                frmO.comboBoxDBLink.Enabled = true;

                frmO.comboBoxEmpresa.DropDownStyle = ComboBoxStyle.DropDownList;
                frmO.comboBoxEmpresa.Items.Clear();
                frmO.comboBoxEmpresa.Text = string.Empty;
                frmO.comboBoxEmpresa.Enabled = true;

                frmO.comboBoxTabelasOrigem.DropDownStyle = ComboBoxStyle.DropDownList;
                frmO.comboBoxTabelasOrigem.Items.Clear();
                frmO.comboBoxTabelasOrigem.Text = string.Empty;
                frmO.comboBoxTabelasOrigem.Enabled = true;

                frmO.textBoxDescricao.Text = string.Empty;
                frmO.textBoxDescricao.Enabled = true;

                frmO.Tag = "NOVO";
                frmO.Text = "Tabelas de Origem (NOVO)";

                frmO.ShowDialog();
            }
            else if (this.treeViewNavegador.SelectedNode.Text.Contains("Tabelas de Destino"))
            {
                frmD.comboBoxTabelasDestino.DropDownStyle = ComboBoxStyle.DropDownList;
                frmD.comboBoxTabelasDestino.Items.Clear();
                frmD.comboBoxTabelasDestino.Text = string.Empty;
                frmD.comboBoxTabelasDestino.Enabled = true;

                frmD.textBoxDescricao.Text = string.Empty;
                frmD.textBoxDescricao.Enabled = true;

                frmD.Tag = "NOVO";
                frmD.Text = "Tabelas de Destino (NOVO)";

                frmD.ShowDialog();
            }
            else if (this.treeViewNavegador.SelectedNode.Text.Contains("Mapeamento"))
            {
                frmM.Limpar_Controles(null, null);

                frmM.Tag = "NOVO";
                frmM.Text = "Mapeamento (NOVO)";

                frmM.textBoxIdMapeamento.Text = String.Empty;
                frmM.listBoxAtributosOrigem.Enabled = true;
                frmM.listBoxAtributosDestino.Enabled = true;
                frmM.listBoxAtributosMapeados.Enabled = true;
                frmM.listViewCriteriosSelecaoOrigem.Enabled = true;

                frmM.buttonOutraOrigem.Enabled = true;
                frmM.buttonIncluirMapeamento.Enabled = true;
                frmM.buttonNovoCriterioSelecaoOrigem.Enabled = true;
                frmM.buttonRemoverMapeamento.Enabled = true;
                frmM.buttonRemoverCriterioSelecaoOrigem.Enabled = true;

                frmM.ShowDialog();
            }

            else if (this.treeViewNavegador.SelectedNode.Text.Contains("Tarefas"))
            {
                frmT.Tag = "NOVO";
                frmT.Text = "Tarefas (NOVO)";

                frmT.textBoxIdTarefa.Enabled = false;
                frmT.textBoxIdMapeamento.Enabled = true;
                frmT.buttonPesquisaIdMap.Enabled = true;
                frmT.textBoxDescricao.Enabled = true;

                frmT.textBoxIdTarefa.Text = string.Empty;
                frmT.textBoxIdMapeamento.Text = string.Empty;
                frmT.textBoxDescricao.Text = string.Empty;

                frmT.checkBoxExecutar.Checked = false;

                frmT.ShowDialog();
            }

            TreeViewEventArgs args = new TreeViewEventArgs(this.treeViewNavegador.SelectedNode);
            this.treeViewNavegador_AfterSelect(this.treeViewNavegador, args);

        }

        private void Editar(object sender, EventArgs e)
        {
            if (this.treeViewNavegador.SelectedNode.Text == "Tabelas de Origem")
            {
                if (this.printableListViewMapWin.FocusedItem != null)
                {
                    frmO.comboBoxDBLink.DropDownStyle = ComboBoxStyle.Simple;
                    frmO.comboBoxDBLink.Enabled = false;

                    frmO.comboBoxEmpresa.DropDownStyle = ComboBoxStyle.Simple;
                    frmO.comboBoxEmpresa.Text = this.printableListViewMapWin.FocusedItem.SubItems[0].Text.PadRight(this.printableListViewMapWin.FocusedItem.SubItems[0].Text.Length) + " " + this.printableListViewMapWin.FocusedItem.SubItems[1].Text.PadRight(printableListViewMapWin.FocusedItem.SubItems[1].Text.Length);
                    frmO.comboBoxEmpresa.Enabled = false;

                    frmO.comboBoxTabelasOrigem.DropDownStyle = ComboBoxStyle.Simple;
                    frmO.comboBoxTabelasOrigem.Text = this.printableListViewMapWin.FocusedItem.SubItems[2].Text;
                    frmO.comboBoxTabelasOrigem.Enabled = false;

                    frmO.textBoxDescricao.Text = this.printableListViewMapWin.FocusedItem.SubItems[3].Text;
                    frmO.textBoxDescricao.Enabled = true;

                    frmO.Tag = "ATUALIZAÇÃO";
                    frmO.Text = "Tabelas de Origem (ATUALIZAÇÃO)";

                    frmO.ShowDialog();
                }
            }
            else if (this.treeViewNavegador.SelectedNode.Text == "Tabelas de Destino")
            {
                if (this.printableListViewMapWin.FocusedItem != null)
                {
                    frmD.comboBoxTabelasDestino.DropDownStyle = ComboBoxStyle.Simple;
                    frmD.comboBoxTabelasDestino.Text = this.printableListViewMapWin.FocusedItem.SubItems[0].Text;
                    frmD.comboBoxTabelasDestino.Enabled = false;

                    frmD.textBoxDescricao.Text = this.printableListViewMapWin.FocusedItem.SubItems[1].Text;
                    frmD.textBoxDescricao.Enabled = true;

                    frmD.Tag = "ATUALIZAÇÃO";
                    frmD.Text = "Tabelas de Destino (ATUALIZAÇÃO)";

                    frmD.ShowDialog();
                }
            }
            else if (this.treeViewNavegador.SelectedNode.Text.Contains("Mapeamento"))
            {

                if (this.printableListViewMapWin.FocusedItem != null)
                {
                    this.Cursor = Cursors.WaitCursor;

                    frmM.textBoxIdMapeamento.Text = this.printableListViewMapWin.FocusedItem.SubItems[0].Text;

                    frmM.comboBoxDBLink.DropDownStyle = ComboBoxStyle.Simple;
                    frmM.comboBoxDBLink.Text = this.printableListViewMapWin.FocusedItem.SubItems[2].Text;
                    frmM.comboBoxDBLink.Enabled = false;

                    frmM.comboBoxEmpresa.DropDownStyle = ComboBoxStyle.Simple;
                    frmM.comboBoxEmpresa.Text = this.printableListViewMapWin.FocusedItem.SubItems[3].Text + " - " + this.printableListViewMapWin.FocusedItem.SubItems[4].Text;
                    frmM.comboBoxEmpresa.Enabled = false;

                    frmM.comboBoxTabelaOrigem.DropDownStyle = ComboBoxStyle.Simple;
                    frmM.comboBoxTabelaOrigem.Text = this.printableListViewMapWin.FocusedItem.SubItems[6].Text;
                    frmM.comboBoxTabelaOrigem_SelectedValueChanged(null, null);
                    frmM.comboBoxTabelaOrigem.Enabled = false;

                    frmM.comboBoxTabelasDestino.DropDownStyle = ComboBoxStyle.Simple;
                    frmM.comboBoxTabelasDestino.Text = this.printableListViewMapWin.FocusedItem.SubItems[8].Text;
                    frmM.comboBoxTabelasDestino_SelectedValueChanged(null, null);
                    frmM.comboBoxTabelasDestino.Enabled = false;

                    frmM.listBoxAtributosOrigem.Enabled = true;
                    frmM.listBoxAtributosDestino.Enabled = true;
                    frmM.listBoxAtributosMapeados.Enabled = true;
                    frmM.listViewCriteriosSelecaoOrigem.Enabled = true;

                    frmM.buttonOutraOrigem.Enabled = true;
                    frmM.buttonIncluirMapeamento.Enabled = true;
                    frmM.buttonNovoCriterioSelecaoOrigem.Enabled = true;
                    frmM.buttonRemoverMapeamento.Enabled = true;
                    frmM.buttonRemoverCriterioSelecaoOrigem.Enabled = true;

                    this.Cursor = Cursors.Default;

                    frmM.Tag = "ATUALIZAÇÃO";
                    frmM.Text = "Mapeamento (ATUALIZAÇÃO)";

                    frmM.ShowDialog();
                }
            }
            else if (this.treeViewNavegador.SelectedNode.Text.Contains("Tarefas"))
            {
                frmT.Tag = "ATUALIZAÇÃO";
                frmT.Text = "Tarefas (ATUALIZAÇÃO)";

                frmT.textBoxIdTarefa.Enabled = false;
                frmT.textBoxIdMapeamento.Enabled = false;
                frmT.buttonPesquisaIdMap.Enabled = false;
                frmT.checkBoxExecutar.Enabled = true;
                frmT.textBoxDescricao.Enabled = true;

                frmT.textBoxIdTarefa.Text = this.printableListViewMapWin.FocusedItem.SubItems[0].Text; ;
                frmT.textBoxIdMapeamento.Text = this.printableListViewMapWin.FocusedItem.SubItems[1].Text; ;
                frmT.textBoxDescricao.Text = this.printableListViewMapWin.FocusedItem.SubItems[2].Text; ;
                frmT.checkBoxExecutar.Checked = false;

                frmT.ShowDialog();
            }

            TreeViewEventArgs args = new TreeViewEventArgs(this.treeViewNavegador.SelectedNode);
            this.treeViewNavegador_AfterSelect(this.treeViewNavegador, args);
        }

        private void Remover(object sender, EventArgs e)
        {
            if (this.treeViewNavegador.SelectedNode.Text == "Tabelas de Origem")
            {
                if (this.printableListViewMapWin.FocusedItem != null)
                {
                    frmO.comboBoxDBLink.DropDownStyle = ComboBoxStyle.Simple;
                    frmO.comboBoxDBLink.Enabled = false;

                    frmO.comboBoxEmpresa.DropDownStyle = ComboBoxStyle.Simple;
                    frmO.comboBoxEmpresa.Text = this.printableListViewMapWin.FocusedItem.SubItems[0].Text.PadRight(this.printableListViewMapWin.FocusedItem.SubItems[0].Text.Length) + " " + this.printableListViewMapWin.FocusedItem.SubItems[1].Text.PadRight(printableListViewMapWin.FocusedItem.SubItems[1].Text.Length);
                    frmO.comboBoxEmpresa.Enabled = false;

                    frmO.comboBoxTabelasOrigem.DropDownStyle = ComboBoxStyle.Simple;
                    frmO.comboBoxTabelasOrigem.Text = this.printableListViewMapWin.FocusedItem.SubItems[2].Text;
                    frmO.comboBoxTabelasOrigem.Enabled = false;

                    frmO.textBoxDescricao.Text = this.printableListViewMapWin.FocusedItem.SubItems[3].Text;
                    frmO.textBoxDescricao.Enabled = false;

                    frmO.Tag = "EXCLUSÃO";
                    frmO.Text = "Tabelas de Origem (EXCLUSÃO)";

                    frmO.ShowDialog();
                }
            }
            else if (this.treeViewNavegador.SelectedNode.Text == "Tabelas de Destino")
            {
                if (this.printableListViewMapWin.FocusedItem != null)
                {
                    frmD.comboBoxTabelasDestino.DropDownStyle = ComboBoxStyle.Simple;
                    frmD.comboBoxTabelasDestino.Text = this.printableListViewMapWin.FocusedItem.SubItems[0].Text;
                    frmD.comboBoxTabelasDestino.Enabled = false;

                    frmD.textBoxDescricao.Text = this.printableListViewMapWin.FocusedItem.SubItems[1].Text;
                    frmD.textBoxDescricao.Enabled = false;

                    frmD.Tag = "EXCLUSÃO";
                    frmD.Text = "Tabelas de Destino (EXCLUSÃO)";

                    frmD.ShowDialog();
                }
            }
            else if (this.treeViewNavegador.SelectedNode.Text.Contains("Mapeamento"))
            {
                if (this.printableListViewMapWin.FocusedItem != null)
                {
                    this.Cursor = Cursors.WaitCursor;

                    frmM.textBoxIdMapeamento.Text = this.printableListViewMapWin.FocusedItem.SubItems[0].Text;

                    frmM.comboBoxDBLink.DropDownStyle = ComboBoxStyle.Simple;
                    frmM.comboBoxDBLink.Text = this.printableListViewMapWin.FocusedItem.SubItems[2].Text;
                    frmM.comboBoxDBLink.Enabled = false;

                    frmM.comboBoxEmpresa.Enabled = false;
                    frmM.comboBoxEmpresa.DropDownStyle = ComboBoxStyle.Simple;
                    frmM.comboBoxEmpresa.Text = this.printableListViewMapWin.FocusedItem.SubItems[3].Text;

                    frmM.comboBoxTabelaOrigem.Enabled = false;
                    frmM.comboBoxTabelaOrigem.DropDownStyle = ComboBoxStyle.Simple;
                    frmM.comboBoxTabelaOrigem.Text = this.printableListViewMapWin.FocusedItem.SubItems[6].Text;
                    frmM.comboBoxTabelaOrigem_SelectedValueChanged(null, null);

                    frmM.comboBoxTabelasDestino.Enabled = false;
                    frmM.comboBoxTabelasDestino.DropDownStyle = ComboBoxStyle.Simple;
                    frmM.comboBoxTabelasDestino.Text = this.printableListViewMapWin.FocusedItem.SubItems[8].Text;
                    frmM.comboBoxTabelasDestino_SelectedValueChanged(null, null);

                    frmM.listBoxAtributosOrigem.Enabled = false;
                    frmM.listBoxAtributosDestino.Enabled = false;
                    frmM.listBoxAtributosMapeados.Enabled = false;
                    frmM.listViewCriteriosSelecaoOrigem.Enabled = false;

                    frmM.buttonOutraOrigem.Enabled = false;
                    frmM.buttonIncluirMapeamento.Enabled = false;
                    frmM.buttonNovoCriterioSelecaoOrigem.Enabled = false;
                    frmM.buttonRemoverMapeamento.Enabled = false;
                    frmM.buttonRemoverCriterioSelecaoOrigem.Enabled = false;

                    this.Cursor = Cursors.Default;

                    frmM.Tag = "EXCLUSÃO";
                    frmM.Text = "Mapeamento (EXCLUSÃO)";

                    frmM.ShowDialog();
                }
            }

            else if (this.treeViewNavegador.SelectedNode.Text.Contains("Tarefas"))
            {
                frmT.Tag = "EXCLUSÃO";
                frmT.Text = "Tarefas (EXCLUSÃO)";

                frmT.textBoxIdTarefa.Enabled = false;
                frmT.textBoxIdMapeamento.Enabled = false;
                frmT.buttonPesquisaIdMap.Enabled = false;
                frmT.textBoxDescricao.Enabled = false;

                frmT.textBoxIdTarefa.Text = this.printableListViewMapWin.FocusedItem.SubItems[0].Text; ;
                frmT.textBoxIdMapeamento.Text = this.printableListViewMapWin.FocusedItem.SubItems[1].Text; ;
                frmT.textBoxDescricao.Text = this.printableListViewMapWin.FocusedItem.SubItems[2].Text; ;
                frmT.checkBoxExecutar.Enabled = false;

                frmT.ShowDialog();
            }

            TreeViewEventArgs args = new TreeViewEventArgs(this.treeViewNavegador.SelectedNode);
            this.treeViewNavegador_AfterSelect(this.treeViewNavegador, args);
        }

        private void Sair(object sender, EventArgs e)
        {
            Close();
        }

        private void ToolBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStrip.Visible = toolBarToolStripMenuItem.Checked;
        }

        private void StatusBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            statusStrip.Visible = statusBarToolStripMenuItem.Checked;
        }

        private void MDIParentMapWin_Load(object sender, EventArgs e)
        {
            this.toolStripStatusLabel.Text = "Aguarde...";
            this.printableListViewMapWin.Dock = DockStyle.Fill;
            this.treeViewNavegador.Nodes["NodeDadosBasicos"].ExpandAll();
            this.treeViewNavegador.Nodes["NodeCargaTabelas"].ExpandAll();
            this.toolStripStatusLabel.Text = "Pronto";

            this.printableListViewMapWin.Visible = true;
            this.printableListViewMapWin.Dock = DockStyle.Fill;

            int inic;
            int fim;

            string dataSource = Database.connection.DatabaseName;

            inic = Database.connection.ConnectionString.IndexOf("User Id");
            fim = Database.connection.ConnectionString.IndexOf(";", inic);
            string username = Database.connection.ConnectionString.Substring(inic, fim - inic).Split('=')[1].Trim().ToLower();

            this.toolStripStatusLabelUsername.Text = dataSource + "/" + username;

            //this.toolStripStatusLabelUsername.Alignment = ToolStripItemAlignment.Left; // = new Padding((int)(this.statusStrip.Width - this.toolStripStatusLabelUsername.Size.Width), 0, 0, 0);

            //this.toolStripComboBoxNumeroRegistros.SelectedIndex = 0;

        }

        private void editMenu_Click(object sender, EventArgs e)
        {

        }

        public void treeViewNavegador_AfterSelect(object sender, TreeViewEventArgs e)
        {
            this.toolStripStatusLabel.Text = "Aguarde...";

            this.statusStrip.Refresh();

            this.printableListViewMapWin.Clear();

            this.printableListViewMapWin.GridLines = false;

            this.printPreviewToolStripMenuItem.Enabled = false;
            this.printToolStripMenuItem.Enabled = false;

            this.newToolStripButton.Enabled = false;
            this.newToolStripMenuItem.Enabled = false;
            this.novoToolStripMenuItem.Enabled = false;

            this.openToolStripButton.Enabled = false;
            this.openToolStripMenuItem.Enabled = false;
            this.abrirToolStripMenuItem.Enabled = false;

            this.toolStripButtonRemove.Enabled = false;
            this.deleteToolStripMenuItem.Enabled = false;
            this.excluirToolStripMenuItem.Enabled = false;

            this.toolStripComboBoxNumeroRegistros.Enabled = false;

            this.toolStripButtonRefresh.Enabled = false;

            string row_num = string.Empty;

            if (this.toolStripComboBoxNumeroRegistros.SelectedIndex == 0)
            {
                row_num = "FETCH NEXT 100 ROWS ONLY";
            }
            else if (this.toolStripComboBoxNumeroRegistros.SelectedIndex == 1)
            {
                row_num = "OFFSET 100 ROWS FETCH NEXT 100 ROWS ONLY";
            }

            if (this.printableListViewMapWin.Groups.Count > 0)
            { 
                // Removes the first group in the collection.
                this.printableListViewMapWin.Groups.RemoveAt(0);
                // Clears all groups.
                this.printableListViewMapWin.Groups.Clear();
            }
            
            this.printableListViewMapWin.View = View.Details;
            this.printableListViewMapWin.Dock = DockStyle.Fill;

            if (e.Node.Text.Contains("Tabelas de Origem"))
            {
                this.Cursor = Cursors.WaitCursor;

                OracleCommand cmd = new OracleCommand();

                cmd.Connection = Database.connection;

                cmd.CommandText = "SELECT id_empresa \"Id. Empresa\", nome_empresa \"Nome da Empresa\", " +
                                         "cod_tabela_orig \"Cód. Tabela Origem\", " +
                                         "desc_tabela_orig \"Descrição\", " +
                                         "id_objeto \"Id. Objeto\" " +
                                  "FROM gsf_tabela_orig";
                cmd.CommandType = CommandType.Text;

                try
                { 
                    OracleDataReader dr = cmd.ExecuteReader();

                    while (dr.Read())
                    {
                        if (this.printableListViewMapWin.Columns.Count == 0)
                        {
                            for (int i = 0; i < dr.FieldCount; i++)
                            {
                                string col = dr.GetName(i).ToString();
                                if (col != "Id. Objeto")
                                {
                                    this.printableListViewMapWin.Columns.Add(col, dr.ToString().Length * 10);
                                }
                            }
                        }

                        ListViewItem lv = new ListViewItem(dr[0].ToString());

                        for (int i = 1; i < dr.FieldCount; i++)
                        {
                            string col = dr[i].ToString();

                            if (col != "Id. Objeto")
                            {
                                lv.SubItems.Add(col);
                            }
                            else
                            {
                                lv.SubItems[i].Tag = col;
                            }
                        }

                        this.printableListViewMapWin.Items.Add(lv);
                    }

                    dr.Close();

                    try
                    { 
                        this.newToolStripButton.Enabled = true;
                        this.newToolStripMenuItem.Enabled = true;
                        this.novoToolStripMenuItem.Enabled = true;

                        if (this.printableListViewMapWin.Items.Count > 0)
                        {
                            this.printableListViewMapWin.GridLines = true;
                            this.printToolStripMenuItem.Enabled = true;
                            this.printPreviewToolStripMenuItem.Enabled = true;
                        }

                        this.toolStripButtonRefresh.Enabled = true;

                        this.Cursor = Cursors.Default;
                        this.newToolStripButton.Enabled = true;
                        this.newToolStripMenuItem.Enabled = true;
                        this.novoToolStripMenuItem.Enabled = true;

                        if (this.printableListViewMapWin.Items.Count > 0)
                        {
                            this.printableListViewMapWin.GridLines = true;
                            this.printToolStripMenuItem.Enabled = true;
                            this.printPreviewToolStripMenuItem.Enabled = true;
                        }

                        this.toolStripButtonRefresh.Enabled = true;

                        this.Cursor = Cursors.Default;
                    }
                    catch (Exception ex)
                    {
                        this.Cursor = Cursors.Default;
                        MessageBox.Show(Environment.NewLine + ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                catch (OracleException ex)
                {
                    this.Cursor = Cursors.Default;
                    MessageBox.Show(Environment.NewLine + ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else if (e.Node.Text.Contains("Tabelas de Destino"))
            {
                this.Cursor = Cursors.WaitCursor;

                OracleCommand cmd = new OracleCommand();

                cmd.Connection = Database.connection;
                cmd.CommandText = "SELECT cod_tabela_dest \"Cód. Tabela Destino\", " +
                                         "desc_tabela_dest \"Descrição\", " +
                                         "id_objeto \"Id. Objeto\" " +
                                  "FROM gsf_tabela_dest";
                cmd.CommandType = CommandType.Text;

                try
                {
                    OracleDataReader dr = cmd.ExecuteReader();

                    while (dr.Read())
                    {
                        if (this.printableListViewMapWin.Columns.Count == 0)
                        {
                            for (int i = 0; i < dr.FieldCount; i++)
                            {
                                string col = dr.GetName(i).ToString();
                                if (col != "Id. Objeto")
                                {
                                    this.printableListViewMapWin.Columns.Add(col, dr.ToString().Length * 10);
                                }
                            }
                        }

                        ListViewItem lv = new ListViewItem(dr[0].ToString());

                        for (int i = 1; i < dr.FieldCount; i++)
                        {
                            string col = dr[i].ToString();

                            if (col != "Id. Objeto")
                            {
                                lv.SubItems.Add(col);
                            }
                            else
                            {
                                lv.SubItems[i].Tag = col;
                            }
                        }

                        this.printableListViewMapWin.Items.Add(lv);
                    }

                    dr.Close();

                    try
                    { 
                        this.newToolStripButton.Enabled = true;
                        this.newToolStripMenuItem.Enabled = true;
                        this.novoToolStripMenuItem.Enabled = true;

                        if (this.printableListViewMapWin.Items.Count > 0)
                        {
                            this.printableListViewMapWin.GridLines = true;
                            this.printToolStripMenuItem.Enabled = true;
                            this.printPreviewToolStripMenuItem.Enabled = true;
                        }

                        this.toolStripButtonRefresh.Enabled = true;

                        this.Cursor = Cursors.Default;
                    }
                    catch (Exception ex)
                    {
                        this.Cursor = Cursors.Default;
                        MessageBox.Show(Environment.NewLine + ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
            }
                catch (OracleException ex)
                {
                    this.Cursor = Cursors.Default;
                    MessageBox.Show(Environment.NewLine + ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            else if (e.Node.Text.Contains("Mapeamento"))
            {
                if (this.toolStripComboBoxNumeroRegistros.SelectedIndex == -1)
                {
                    this.toolStripComboBoxNumeroRegistros.SelectedIndex = 0;
                }

                this.Cursor = Cursors.WaitCursor;

                this.toolStripComboBoxNumeroRegistros.Enabled = true;

                OracleCommand cmd = new OracleCommand();

                cmd.Connection = Database.connection;
                cmd.CommandText = "SELECT " +
                                        "id_map \"Id. Mapeamento\", " +
                                        "TO_CHAR(data_map, 'DD/MM/YYYY HH24:MI:SS') \"Data Mapeamento\", " +
                                        "id_db_link \"Id. DBLink\", " +
                                        "id_empresa \"Id. Empresa\", " +
                                        "\"Nome da Empresa\", " +
                                        "cod_modulo \"Cód. Modulo\", " +
                                        "cod_tabela_orig \"Tabela Origem\", " +
                                        "\"Descrição da Tabela de Origem\", " +
                                        "cod_tabela_dest \"Tabela Destino\", " +
                                        "\"Descrição da Tabela de Destino\", " +
                                        "id_objeto \"Id. Objeto\" " +
                                  "FROM   gsf_map t ORDER BY t.VERSAO_LINHA DESC " + row_num;
                cmd.CommandType = CommandType.Text;

                try
                { 
                    OracleDataReader dr = cmd.ExecuteReader();

                    while (dr.Read())
                    {
                        if (this.printableListViewMapWin.Columns.Count == 0)
                        {
                            for (int i = 0; i < dr.FieldCount; i++)
                            {
                                string col = dr.GetName(i).ToString();
                                if (col != "Id. Objeto")
                                {
                                    this.printableListViewMapWin.Columns.Add(col, dr.ToString().Length * 10);
                                }
                            }
                        }

                        ListViewItem lv = new ListViewItem(dr[0].ToString());

                        for (int i = 1; i < dr.FieldCount; i++)
                        {
                            string col = dr[i].ToString();

                            if (col != "Id. Objeto")
                            {
                                lv.SubItems.Add(col);
                            }
                            else
                            {
                                lv.SubItems[i].Tag = col;
                            }
                        }

                        this.printableListViewMapWin.Items.Add(lv);
                        }

                    dr.Close();

                    try
                    { 
                        this.newToolStripButton.Enabled = true;
                        this.newToolStripMenuItem.Enabled = true;
                        this.novoToolStripMenuItem.Enabled = true;

                        if (this.printableListViewMapWin.Items.Count > 0)
                        {
                            this.printableListViewMapWin.GridLines = true;
                            this.printToolStripMenuItem.Enabled = true;
                            this.printPreviewToolStripMenuItem.Enabled = true;
                        }

                        this.toolStripButtonRefresh.Enabled = true;

                        this.Cursor = Cursors.Default;
                    }
                    catch (Exception ex)
                    {
                        this.Cursor = Cursors.Default;
                        MessageBox.Show(Environment.NewLine + ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                catch (OracleException ex)
                {
                    this.Cursor = Cursors.Default;
                    MessageBox.Show(Environment.NewLine + ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else if (e.Node.Text.Contains("Tarefas"))
            {
                if (this.toolStripComboBoxNumeroRegistros.SelectedIndex == -1)
                {
                    this.toolStripComboBoxNumeroRegistros.SelectedIndex = 0;
                }

                this.Cursor = Cursors.WaitCursor;

                this.toolStripComboBoxNumeroRegistros.Enabled = true;

                OracleCommand cmd = new OracleCommand();

                cmd.Connection = Database.connection;
                cmd.CommandText = "SELECT t.id_tarefa \"Id. Tarefa\", " +
                                         "t.id_map \"Id. Mapeamento\", " +
                                         "t.desc_tarefa  \"Descrição da Tarefa\", " +
                                         "TO_CHAR(t.data_inicio, 'DD/MM/YYYY HH24:MI:SS') \"Data de Início\", " +
                                         "TO_CHAR(t.data_termino, 'DD/MM/YYYY HH24:MI:SS') \"Data de Término\", " +
                                         "t.status \"Status\", " +
                                         "t.id_objeto \"Id. Objeto\"" +
                                  "FROM   gsf_tarefa t ORDER BY t.VERSAO_LINHA DESC " + row_num;
                cmd.CommandType = CommandType.Text;

                try
                {
                    OracleDataReader dr = cmd.ExecuteReader();

                    while (dr.Read())
                    {
                        if (this.printableListViewMapWin.Columns.Count == 0)
                        {
                            for (int i = 0; i < dr.FieldCount; i++)
                            {
                                string col = dr.GetName(i).ToString();
                                if (col != "Id. Objeto")
                                {
                                    this.printableListViewMapWin.Columns.Add(col, dr.ToString().Length * 10);
                                }
                            }
                        }

                        ListViewItem lv = new ListViewItem(dr[0].ToString());

                        for (int i = 1; i < dr.FieldCount; i++)
                        {
                            string col = dr[i].ToString();

                            if (col != "Id. Objeto")
                            {
                                lv.SubItems.Add(col);
                            }
                            else
                            {
                                lv.SubItems[i].Tag = col;
                            }
                        }

                        this.printableListViewMapWin.Items.Add(lv);
                    }

                    dr.Close();

                    try
                    { 
                        this.newToolStripButton.Enabled = true;
                        this.newToolStripMenuItem.Enabled = true;
                        this.novoToolStripMenuItem.Enabled = true;

                        if (this.printableListViewMapWin.Items.Count > 0)
                        {
                            this.printableListViewMapWin.GridLines = true;
                            this.printToolStripMenuItem.Enabled = true;
                            this.printPreviewToolStripMenuItem.Enabled = true;
                        }

                        this.toolStripButtonRefresh.Enabled = true;

                        this.Cursor = Cursors.Default;
                    }
                    catch (Exception ex)
                    {
                        this.Cursor = Cursors.Default;
                        MessageBox.Show(Environment.NewLine + ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                catch (OracleException ex)
                {
                    this.Cursor = Cursors.Default;
                    MessageBox.Show(Environment.NewLine + ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            else if (e.Node.Text.Contains("Logs"))
            {
                if (this.toolStripComboBoxNumeroRegistros.SelectedIndex == -1)
                {
                    this.toolStripComboBoxNumeroRegistros.SelectedIndex = 0;
                }

                this.Cursor = Cursors.WaitCursor;

                this.toolStripComboBoxNumeroRegistros.Enabled = true;

                OracleCommand cmd = new OracleCommand();

                cmd.Connection = Database.connection;
                cmd.CommandText = "SELECT " +
                                          "ID_LOG \"Id. Log\", " +
                                          "DESCRICAO_TIPO_LOG \"Descrição do Tipo de Log\", " +
                                          "TEXTO_LOG \"Texto\", " +
                                          "id_objeto \"Id. Objeto\" " +
                                  "FROM   gsf_log " +
                                  "WHERE  UPPER(DS_METODO) = UPPER('GSF_IMPORTACAO_PKG.Executar_Carga_Dados_Tarefa') ORDER BY VERSAO_LINHA DESC " + row_num;
                cmd.CommandType = CommandType.Text;

                try
                { 
                    OracleDataReader dr = cmd.ExecuteReader();

                    while (dr.Read())
                    {
                        if (this.printableListViewMapWin.Columns.Count == 0)
                        {
                            for (int i = 0; i < dr.FieldCount; i++)
                            {
                                string col = dr.GetName(i).ToString();
                            
                                if (col != "Id. Objeto")
                                {
                                    this.printableListViewMapWin.Columns.Add(col, dr.ToString().Length * 10);
                                }
                            }
                        }

                        ListViewItem lv = new ListViewItem(dr[0].ToString());

                        for (int i = 1; i < dr.FieldCount; i++)
                        {
                            string col = dr[i].ToString();

                            if (col != "Id. Objeto")
                            {
                                lv.SubItems.Add(col);
                            }
                            else
                            {
                                lv.SubItems[i].Tag = col;
                            }
                        }

                        this.printableListViewMapWin.Items.Add(lv);
                    }

                    dr.Close();

                    try
                    { 
                        ListView lsv = this.printableListViewMapWin;

                        Util.GroupListView(lsv, 1);

                        if (this.printableListViewMapWin.Items.Count > 0)
                        {
                            this.printableListViewMapWin.GridLines = true;
                            this.printToolStripMenuItem.Enabled = true;
                            this.printPreviewToolStripMenuItem.Enabled = true;
                        }

                        this.toolStripButtonRefresh.Enabled = true;

                        this.Cursor = Cursors.Default;
                    }
                    catch (Exception ex)
                    {
                        this.Cursor = Cursors.Default;
                        MessageBox.Show(Environment.NewLine + ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                catch (OracleException ex)
                {
                    this.Cursor = Cursors.Default;
                    MessageBox.Show(Environment.NewLine + ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            if (e.Node.Text.Contains("Tabelas de Origem") || e.Node.Text.Contains("Tabelas de Destino"))
            {
                this.printableListViewMapWin.Sorting = SortOrder.Descending;
            }
            else
            {
                this.printableListViewMapWin.Sorting = SortOrder.Ascending;
            }

            ColumnClickEventArgs args = new ColumnClickEventArgs(0);

            printableListViewMapWin_ColumnClick(this.printableListViewMapWin, args);

            this.toolStripStatusLabel.Text = "Pronto";

            this.statusStrip.Refresh();
        }

        private void MDIParentMapWin_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }

        private void MDIParentMapWin_Shown(object sender, EventArgs e)
        {
            
        }

        private void toolStripButtonRefresh_Click(object sender, EventArgs e)
        {
            if (Database.connection.State == ConnectionState.Open)
            {
                this.treeViewNavegador_AfterSelect(this.treeViewNavegador, new TreeViewEventArgs(this.treeViewNavegador.SelectedNode));
            }
        }

        private void printableListViewMapWin_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            // Determine whether the column is the same as the last column clicked.
            if (e.Column != sortColumn)
            {
                // Set the sort column to the new column.
                sortColumn = e.Column;
                // Set the sort order to ascending by default.
                printableListViewMapWin.Sorting = SortOrder.Ascending;
            }
            else
            {
                // Determine what the last sort order was and change it.
                if (printableListViewMapWin.Sorting == SortOrder.Ascending)
                    printableListViewMapWin.Sorting = SortOrder.Descending;
                else
                    printableListViewMapWin.Sorting = SortOrder.Ascending;
            }

            // Call the sort method to manually sort.
            printableListViewMapWin.Sort();

            this.printableListViewMapWin.ListViewItemSorter = new ListViewAutoSorter(e.Column,
                                                                            printableListViewMapWin.Sorting);

        }

        private void printableListViewMapWin_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void printableListViewMapWin_Click(object sender, EventArgs e)
        {

        }

        private void printableListViewMapWin_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (this.printableListViewMapWin.FocusedItem != null)
                {
                    ListViewItemSelectionChangedEventArgs args = new ListViewItemSelectionChangedEventArgs(this.printableListViewMapWin.FocusedItem, this.printableListViewMapWin.FocusedItem.Index, true);
                    printableListViewMapWin_ItemSelectionChanged(this.printableListViewMapWin, args);
                }

                contextMenuStrip.Show(Cursor.Position);
                contextMenuStrip.Visible = true;
            }
        }

        private void printableListViewMapWin_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.Editar(null, null);
            }

        }

        private void MDIParentMapWin_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }

        }

        private void printSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PrintDialog prt = new PrintDialog();

            prt.ShowDialog();
        }

        private void printPreviewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.printableListViewMapWin.Title = this.treeViewNavegador.SelectedNode.Text;
            this.printableListViewMapWin.FitToPage = true;
            this.printableListViewMapWin.Height = this.Height;
            this.printableListViewMapWin.Width = this.Width;

            //Point p = new Point(Owner.Left + Owner.Width / 2 - Width / 2 + offset, Owner.Top + Owner.Height / 2 - Height / 2 + offset);
            //this.Location = p;

            this.printableListViewMapWin.PrintPreview();
        }

        private void splitContainer1_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void configurarPáginaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            printableListViewMapWin.PageSetup();
        }

        private void helpMenu_Click(object sender, EventArgs e)
        {

        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBoxMapWinApp frmA = new AboutBoxMapWinApp();

            frmA.ShowDialog();

        }

        private void printableListViewMapWin_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (e.IsSelected)
            { 
                this.newToolStripButton.Enabled = true;
                this.newToolStripMenuItem.Enabled = true;
                this.novoToolStripMenuItem.Enabled = true;

                this.openToolStripButton.Enabled = true;
                this.openToolStripMenuItem.Enabled = true;
                this.abrirToolStripMenuItem.Enabled = true;

                this.toolStripButtonRemove.Enabled = true;
                this.deleteToolStripMenuItem.Enabled = true;
                this.excluirToolStripMenuItem.Enabled = true;
            }
            else
            {
                this.newToolStripButton.Enabled = true;
                this.newToolStripMenuItem.Enabled = true;
                this.novoToolStripMenuItem.Enabled = true;

                this.openToolStripButton.Enabled = false;
                this.openToolStripMenuItem.Enabled = false;
                this.abrirToolStripMenuItem.Enabled = false;

                this.toolStripButtonRemove.Enabled = false;
                this.deleteToolStripMenuItem.Enabled = false;
                this.excluirToolStripMenuItem.Enabled = false;

            }
        }

        private void printToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.printableListViewMapWin.Title = this.treeViewNavegador.SelectedNode.Text;
            this.printableListViewMapWin.FitToPage = true;
            this.printableListViewMapWin.Height = this.Height;
            this.printableListViewMapWin.Width = this.Width;

            this.printableListViewMapWin.Print();
        }

        private void contextMenuStrip_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }
    }
}

