﻿using System.Windows.Forms;

namespace MapWinApp
{
    partial class FormTabelasOrigem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormTabelasOrigem));
            this.buttonOK = new System.Windows.Forms.Button();
            this.labelEmpresa = new System.Windows.Forms.Label();
            this.comboBoxEmpresa = new System.Windows.Forms.ComboBox();
            this.comboBoxDBLink = new System.Windows.Forms.ComboBox();
            this.labelDBLink = new System.Windows.Forms.Label();
            this.comboBoxTabelasOrigem = new System.Windows.Forms.ComboBox();
            this.labelCodTabela = new System.Windows.Forms.Label();
            this.textBoxDescricao = new System.Windows.Forms.TextBox();
            this.labelDescricao = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonOK
            // 
            this.buttonOK.Enabled = false;
            this.buttonOK.Location = new System.Drawing.Point(328, 220);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 8;
            this.buttonOK.Text = "OK";
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // labelEmpresa
            // 
            this.labelEmpresa.AutoSize = true;
            this.labelEmpresa.Location = new System.Drawing.Point(12, 59);
            this.labelEmpresa.Name = "labelEmpresa";
            this.labelEmpresa.Size = new System.Drawing.Size(51, 13);
            this.labelEmpresa.TabIndex = 2;
            this.labelEmpresa.Text = "Empresa:";
            // 
            // comboBoxEmpresa
            // 
            this.comboBoxEmpresa.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxEmpresa.FormattingEnabled = true;
            this.comboBoxEmpresa.Location = new System.Drawing.Point(15, 75);
            this.comboBoxEmpresa.Name = "comboBoxEmpresa";
            this.comboBoxEmpresa.Size = new System.Drawing.Size(293, 21);
            this.comboBoxEmpresa.TabIndex = 3;
            this.comboBoxEmpresa.DropDown += new System.EventHandler(this.comboBoxEmpresa_DropDown);
            this.comboBoxEmpresa.SelectedIndexChanged += new System.EventHandler(this.comboBoxEmpresa_SelectedIndexChanged);
            this.comboBoxEmpresa.SelectedValueChanged += new System.EventHandler(this.comboBoxDBLink_SelectedValueChanged);
            // 
            // comboBoxDBLink
            // 
            this.comboBoxDBLink.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDBLink.FormattingEnabled = true;
            this.comboBoxDBLink.Location = new System.Drawing.Point(15, 25);
            this.comboBoxDBLink.Name = "comboBoxDBLink";
            this.comboBoxDBLink.Size = new System.Drawing.Size(188, 21);
            this.comboBoxDBLink.TabIndex = 1;
            this.comboBoxDBLink.DropDown += new System.EventHandler(this.comboBoxDBLink_DropDown);
            this.comboBoxDBLink.SelectedIndexChanged += new System.EventHandler(this.comboBoxDBLink_SelectedIndexChanged);
            this.comboBoxDBLink.SelectedValueChanged += new System.EventHandler(this.comboBoxDBLink_SelectedValueChanged);
            // 
            // labelDBLink
            // 
            this.labelDBLink.AutoSize = true;
            this.labelDBLink.Location = new System.Drawing.Point(12, 9);
            this.labelDBLink.Name = "labelDBLink";
            this.labelDBLink.Size = new System.Drawing.Size(48, 13);
            this.labelDBLink.TabIndex = 0;
            this.labelDBLink.Text = "DB Link:";
            // 
            // comboBoxTabelasOrigem
            // 
            this.comboBoxTabelasOrigem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTabelasOrigem.FormattingEnabled = true;
            this.comboBoxTabelasOrigem.Location = new System.Drawing.Point(15, 125);
            this.comboBoxTabelasOrigem.Name = "comboBoxTabelasOrigem";
            this.comboBoxTabelasOrigem.Size = new System.Drawing.Size(250, 21);
            this.comboBoxTabelasOrigem.TabIndex = 5;
            this.comboBoxTabelasOrigem.DropDown += new System.EventHandler(this.comboBoxTabelasOrigem_DropDown);
            this.comboBoxTabelasOrigem.SelectedIndexChanged += new System.EventHandler(this.comboBoxTabelasOrigem_SelectedIndexChanged);
            this.comboBoxTabelasOrigem.SelectedValueChanged += new System.EventHandler(this.comboBoxDBLink_SelectedValueChanged);
            // 
            // labelCodTabela
            // 
            this.labelCodTabela.AutoSize = true;
            this.labelCodTabela.Location = new System.Drawing.Point(12, 110);
            this.labelCodTabela.Name = "labelCodTabela";
            this.labelCodTabela.Size = new System.Drawing.Size(43, 13);
            this.labelCodTabela.TabIndex = 4;
            this.labelCodTabela.Text = "Tabela:";
            // 
            // textBoxDescricao
            // 
            this.textBoxDescricao.Location = new System.Drawing.Point(15, 177);
            this.textBoxDescricao.Name = "textBoxDescricao";
            this.textBoxDescricao.Size = new System.Drawing.Size(388, 20);
            this.textBoxDescricao.TabIndex = 7;
            this.textBoxDescricao.TextChanged += new System.EventHandler(this.comboBoxDBLink_SelectedValueChanged);
            // 
            // labelDescricao
            // 
            this.labelDescricao.AutoSize = true;
            this.labelDescricao.Location = new System.Drawing.Point(12, 161);
            this.labelDescricao.Name = "labelDescricao";
            this.labelDescricao.Size = new System.Drawing.Size(58, 13);
            this.labelDescricao.TabIndex = 6;
            this.labelDescricao.Text = "Descrição:";
            // 
            // FormTabelasOrigem
            // 
            this.AcceptButton = this.buttonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(411, 255);
            this.Controls.Add(this.labelDescricao);
            this.Controls.Add(this.textBoxDescricao);
            this.Controls.Add(this.comboBoxTabelasOrigem);
            this.Controls.Add(this.labelCodTabela);
            this.Controls.Add(this.comboBoxDBLink);
            this.Controls.Add(this.labelDBLink);
            this.Controls.Add(this.comboBoxEmpresa);
            this.Controls.Add(this.labelEmpresa);
            this.Controls.Add(this.buttonOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormTabelasOrigem";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Tabelas de Origem";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormTabelasOrigem_FormClosing);
            this.Load += new System.EventHandler(this.FormTabelasOrigem_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormTabelasOrigem_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonOK;
        private Label labelEmpresa;
        public ComboBox comboBoxEmpresa;
        public ComboBox comboBoxDBLink;
        private Label labelDBLink;
        public ComboBox comboBoxTabelasOrigem;
        private Label labelCodTabela;
        public TextBox textBoxDescricao;
        private Label labelDescricao;
    }
}