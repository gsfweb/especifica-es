create or replace view gsf_tarefa as
select ID_TAREFA
     , ID_MAP
     , GSF_EMPRESA_PKG.Obtem_Nome_Empresa(GSF_MAP_PKG.obtem_id_empresa(ID_MAP)) NOME_EMPRESA
     , DESC_TAREFA
     , DATA_INICIO
     , DATA_TERMINO
     , STATUS
     , ID_OBJETO
     , VERSAO_LINHA
 from GSF_TAREFA_TAB
WITH READ ONLY;
