create or replace view gsf_map as
select ID_MAP
     , ID_EMPRESA
     , Gsf_Empresa_Pkg.Obtem_Nome_Empresa(ID_EMPRESA) "Nome da Empresa"
     , ID_DB_LINK
     , COD_MODULO
     , COD_TABELA_ORIG
     , Gsf_Tabela_Orig_Pkg.Obtem_Descricao(ID_EMPRESA, COD_TABELA_ORIG) "Descri��o da Tabela de Origem"
     , COD_TABELA_DEST
     , Gsf_Tabela_Dest_Pkg.Obtem_Descricao(COD_TABELA_DEST) "Descri��o da Tabela de Destino"
     , VERSAO_LINHA
     , ID_OBJETO
     , DATA_MAP
 from GSF_MAP_TAB
WITH READ ONLY;
