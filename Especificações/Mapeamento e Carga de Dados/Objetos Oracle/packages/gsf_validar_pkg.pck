create or replace package gsf_validar_pkg is

procedure verifica_validacao(p_id_map gsf_map_tab.id_map%TYPE);

procedure validacao_tabela_campo( p_id_map          gsf_map_tab.id_map%TYPE
                                , p_cod_tabela_dest gsf_map_tab.cod_tabela_dest%TYPE
                                , p_cod_atr_dest    gsf_atr_map_tab.cod_atr_dest%TYPE );

--remove mascara cnpj ou cpf                                    
procedure valida_cnpj_cpf( p_id_map          gsf_map_tab.id_map%TYPE
                         , p_cod_tabela_dest gsf_map_tab.cod_tabela_dest%TYPE
                         , p_cod_atr_dest    gsf_atr_map_tab.cod_atr_dest%TYPE );            
                         
--preenche campo ind_pessoa_fisica_juridica - tabela CLIENTE_FORNECEDOR_DEST                          
procedure pree_campo_ind_pessoa_fis_jur( p_id_map          gsf_map_tab.id_map%TYPE
                                       , p_cod_tabela_dest gsf_map_tab.cod_tabela_dest%TYPE
                                       , p_cod_atr_dest    gsf_atr_map_tab.cod_atr_dest%TYPE
                                       , p_cod_atr_aux     gsf_atr_map_tab.cod_atr_dest%TYPE );                                                

end gsf_validar_pkg;
/
create or replace package body gsf_validar_pkg is

--variaveis globais
v_sql_dynamic clob; 


procedure verifica_validacao(p_id_map gsf_map_tab.id_map%TYPE) is

       v_id_map gsf_atr_map_tab.id_map%TYPE; 
       v_tabela gsf_map_tab.cod_tabela_dest%TYPE;
       v_campo  gsf_atr_map_tab.cod_atr_dest%TYPE;

       CURSOR c_tabelas_campos is 
       select map.cod_tabela_dest,
              atr.cod_atr_dest
         from gsf_map_tab map
            , gsf_atr_map_tab atr 
        where map.id_map = atr.id_map
          and map.id_map = v_id_map
        order by
              atr.id_ordem;
          
       TYPE t_tabelas_campos IS TABLE OF c_tabelas_campos%ROWTYPE 
                             INDEX BY PLS_INTEGER;
            
       l_tabelas_campos t_tabelas_campos;   
          
       begin
         
         v_id_map := p_id_map;  
         
         OPEN c_tabelas_campos;

         FETCH c_tabelas_campos

         BULK COLLECT INTO l_tabelas_campos;

         CLOSE c_tabelas_campos;
         
         FOR indx IN l_tabelas_campos.first .. l_tabelas_campos.last LOOP
           
           v_tabela := l_tabelas_campos(indx).cod_tabela_dest;
           v_campo  := l_tabelas_campos(indx).cod_atr_dest;
           
           validacao_tabela_campo( v_id_map
                                 , v_tabela
                                 , v_campo );
           
         END LOOP;
         
         EXCEPTION
           WHEN others THEN
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'gsf_validadar_pkg.verifica_validacao');
             dbms_output.put_line(SQLERRM);
         
end verifica_validacao;

procedure validacao_tabela_campo( p_id_map          gsf_map_tab.id_map%TYPE
                                , p_cod_tabela_dest gsf_map_tab.cod_tabela_dest%TYPE
                                , p_cod_atr_dest    gsf_atr_map_tab.cod_atr_dest%TYPE ) is
                                                     
       v_cod_tabela_dest gsf_map_tab.cod_tabela_dest%TYPE;
       v_cod_atr_dest    gsf_atr_map_tab.cod_atr_dest%TYPE;
       v_id_map          gsf_map_tab.id_map%TYPE;
       
       begin
         
         v_cod_tabela_dest := trim(upper(p_cod_tabela_dest));
         v_cod_atr_dest    := trim(upper(p_cod_atr_dest));
         v_id_map          := p_id_map;
         
            
         CASE v_cod_tabela_dest
           WHEN 'GSF_CLIENTE_FORNECEDOR_DEST' THEN 
             CASE v_cod_atr_dest
               --consolida dado cpf_cnpj, remove mascara 
               WHEN 'CPF_CNPJ' THEN 
                 valida_cnpj_cpf( v_id_map
                                , v_cod_tabela_dest
                                , v_cod_atr_dest );
                                
                 --preenc. campo ind_fisica_juridica ( INFOR. N�O MAPEADA )dependente do camp. cpf_cnpj                 
                 pree_campo_ind_pessoa_fis_jur( v_id_map
                                              , v_cod_tabela_dest
                                              , v_cod_atr_dest
                                              , 'IND_FISICA_JURIDICA' ); 
                                                                                                                      
               ELSE NULL;
                                   
             END CASE;
             
            /*...
              validacao de outras tabelas
              ....*/
              
           ELSE NULL;   
             
         END CASE; 
         
         EXCEPTION
           WHEN others THEN
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'gsf_validadar_pkg.validacao_tabela_campo');
             dbms_output.put_line(SQLERRM);   
            
end validacao_tabela_campo;


procedure valida_cnpj_cpf( p_id_map          gsf_map_tab.id_map%TYPE
                         , p_cod_tabela_dest gsf_map_tab.cod_tabela_dest%TYPE
                         , p_cod_atr_dest    gsf_atr_map_tab.cod_atr_dest%TYPE ) is
                                                

       v_id_map          gsf_map_tab.id_map%TYPE;
       v_cod_tabela_dest gsf_map_tab.cod_tabela_dest%TYPE;
       v_cod_atr_dest    gsf_atr_map_tab.cod_atr_dest%TYPE;
       
       begin
         
       v_id_map 	       := p_id_map;
       v_cod_tabela_dest := p_cod_tabela_dest;
       v_cod_atr_dest    := p_cod_atr_dest;

       v_sql_dynamic := 'update '|| v_cod_tabela_dest|| 
                          ' set '||v_cod_atr_dest ||'= replace(replace(replace('||v_cod_atr_dest||',''.'',''''),''/'',''''),''-'','''')
                          where id_map = '||v_id_map;   


       
       EXECUTE IMMEDIATE v_sql_dynamic;
           
       COMMIT;
       
       EXCEPTION
           WHEN others THEN
             ROLLBACK;   
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'gsf_validadar_pkg.valida_cnpj_cpf');
             dbms_output.put_line(SQLERRM);                                                   
                            
                            
                            
end valida_cnpj_cpf;


procedure pree_campo_ind_pessoa_fis_jur( p_id_map          gsf_map_tab.id_map%TYPE
                                       , p_cod_tabela_dest gsf_map_tab.cod_tabela_dest%TYPE
                                       , p_cod_atr_dest    gsf_atr_map_tab.cod_atr_dest%TYPE
                                       , p_cod_atr_aux     gsf_atr_map_tab.cod_atr_dest%TYPE ) is
                                       
       
       v_id_map          gsf_map_tab.id_map%TYPE;
       v_cod_tabela_dest gsf_map_tab.cod_tabela_dest%TYPE;
       v_cod_atr_dest    gsf_atr_map_tab.cod_atr_dest%TYPE;   
       v_cod_atr_aux     gsf_atr_map_tab.cod_atr_dest%TYPE;
       
       begin
       
         v_id_map          := p_id_map;
         v_cod_tabela_dest := p_cod_tabela_dest;
         v_cod_atr_dest    := p_cod_atr_dest;
         v_cod_atr_aux     := p_cod_atr_aux;
         
         v_sql_dynamic := 'update '|| v_cod_tabela_dest|| 
                          ' set '||v_cod_atr_aux ||'= decode(length('||v_cod_atr_dest||'),14,''J'',''F'')
                          where id_map = '||v_id_map;  
                          
         EXECUTE IMMEDIATE v_sql_dynamic;                   
                                                  
         COMMIT;
         
         EXCEPTION
           WHEN others THEN
             ROLLBACK;   
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'gsf_validadar_pkg.pree_campo_ind_pessoa_fis_jur');
             dbms_output.put_line(SQLERRM);                                   

end pree_campo_ind_pessoa_fis_jur; 


end gsf_validar_pkg;
/
