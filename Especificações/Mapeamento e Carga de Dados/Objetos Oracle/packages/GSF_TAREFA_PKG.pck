CREATE OR REPLACE PACKAGE GSF_TAREFA_PKG is

procedure inserir( p_id_map          gsf_tarefa_tab.id_map%TYPE
                 , p_data_inicio     gsf_tarefa_tab.data_termino%TYPE
                 , p_desc_tarefa     gsf_tarefa_tab.desc_tarefa%TYPE
                 , p_status          gsf_tarefa_tab.status%TYPE 
                 , ret_id_tarefa OUT gsf_tarefa_tab.id_tarefa%TYPE);   
                 
procedure alterar( p_id_tarefa    gsf_tarefa_tab.id_tarefa%TYPE
                 , p_data_termino gsf_tarefa_tab.data_termino%TYPE
                 , p_status       gsf_tarefa_tab.status%TYPE );    

function obtem_id_map( p_id_tarefa gsf_atr_map_tab.id_map%TYPE ) return number; 

function obtem_desc_tarefa( p_id_tarefa gsf_atr_map_tab.id_map%TYPE ) return varchar2;  

function obtem_status( p_id_tarefa gsf_atr_map_tab.id_map%TYPE ) return varchar2;                              


END GSF_TAREFA_PKG;
/
CREATE OR REPLACE PACKAGE BODY GSF_TAREFA_PKG IS

v_sql_dynamic varchar2(2000);

procedure inserir( p_id_map          gsf_tarefa_tab.id_map%TYPE
                 , p_data_inicio     gsf_tarefa_tab.data_termino%TYPE
                 , p_desc_tarefa     gsf_tarefa_tab.desc_tarefa%TYPE
                 , p_status          gsf_tarefa_tab.status%TYPE 
                 , ret_id_tarefa OUT gsf_tarefa_tab.id_tarefa%TYPE ) is
                  
      v_id_tarefa   gsf_tarefa_tab.id_tarefa%TYPE;
      v_id_map      gsf_tarefa_tab.id_map%TYPE;
      v_desc_tarefa gsf_tarefa_tab.desc_tarefa%TYPE;
      v_data_inicio gsf_tarefa_tab.data_inicio%TYPE;
      v_status      gsf_tarefa_tab.status%TYPE;
      v_id_objeto   gsf_tarefa_tab.id_objeto%TYPE;
      
      begin 
        
      v_id_map      := p_id_map;
      v_data_inicio := p_data_inicio;
      v_desc_tarefa := p_desc_tarefa;
      v_status      := p_status;
      
      select gsf_id_tarefa_seq.nextval
        INTO v_id_tarefa
        from dual;
      
      ret_id_tarefa := v_id_tarefa;   
      
      v_sql_dynamic := 'INSERT INTO GSF_TAREFA_TAB( ID_TAREFA
                                                 , ID_MAP
                                                 , DESC_TAREFA
                                                 , DATA_INICIO
                                                 , STATUS
                                                 , VERSAO_LINHA )
                                          values ( :id_tarefa
                                                 , :id_map
                                                 , :desc_tarefa
                                                 , :data_inicio
                                                 , :status
                                                 , sysdate )';
      
      
      EXECUTE IMMEDIATE v_sql_dynamic
      USING v_id_tarefa
          , v_id_map
          , v_desc_tarefa
          , v_data_inicio
          , v_status;
      
      
      select rowid 
        INTO v_id_objeto 
        from gsf_tarefa_tab 
       where id_tarefa = v_id_tarefa;
       
      v_sql_dynamic := 'UPDATE GSF_TAREFA_TAB 
                           SET ID_OBJETO = :id_objeto
                         WHERE ID_TAREFA = :id_tarefa';
      
      EXECUTE IMMEDIATE v_sql_dynamic
      USING v_id_objeto
          , v_id_tarefa;
          
      COMMIT;
      
      EXCEPTION
        WHEN OTHERS THEN
          ROLLBACK;
          GSF_LOG_PKG.Inserir('EO',SQLERRM,'GSF_TAREFA.INSERIR');
          dbms_output.put_line(SQLERRM);      
                         

end inserir;

procedure alterar( p_id_tarefa    gsf_tarefa_tab.id_tarefa%TYPE
                 , p_data_termino gsf_tarefa_tab.data_termino%TYPE
                 , p_status       gsf_tarefa_tab.status%TYPE ) is
  
       v_id_tarefa    gsf_tarefa_tab.id_tarefa%TYPE;
       v_data_termino gsf_tarefa_tab.data_termino%TYPE;
       v_status       gsf_tarefa_tab.status%TYPE;
       
       begin
         
         v_id_tarefa    := p_id_tarefa;
         v_data_termino := p_data_termino;
         v_status       := p_status;
         
         v_sql_dynamic := 'UPDATE GSF_TAREFA_TAB
                              SET DATA_TERMINO = :data_termino
                                , STATUS       = :status
                            WHERE ID_TAREFA    = :id_tarefa';
                            
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_data_termino
             , v_status
             , v_id_tarefa;
         
         COMMIT;
         
         EXCEPTION
           WHEN OTHERS THEN
             ROLLBACK;
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'GSF_TAREFA.ALTERAR');
             dbms_output.put_line(SQLERRM);       

                 
end alterar;


function obtem_id_map( p_id_tarefa gsf_atr_map_tab.id_map%TYPE ) return number is
  
       v_id_tarefa gsf_atr_map_tab.id_map%TYPE;
       v_id_map    gsf_map_tab.id_map%TYPE;
       
       begin
          
         v_id_tarefa := p_id_tarefa;
       
         SELECT ID_MAP
           INTO v_id_map
           FROM GSF_TAREFA_TAB
          WHERE ID_TAREFA = v_id_tarefa;   
         
         return v_id_map;  
  
end obtem_id_map;


function obtem_desc_tarefa( p_id_tarefa gsf_atr_map_tab.id_map%TYPE ) return varchar2 is
  
       v_id_tarefa   gsf_atr_map_tab.id_map%TYPE;
       v_desc_tarefa gsf_tarefa_tab.desc_tarefa%TYPE;
       
       begin
         
         v_id_tarefa := p_id_tarefa;
         
         SELECT DESC_TAREFA
           INTO v_desc_tarefa
           FROM GSF_TAREFA_TAB
          WHERE ID_TAREFA = v_id_tarefa;   
          
         return v_desc_tarefa; 
  
end obtem_desc_tarefa;



function obtem_status( p_id_tarefa gsf_atr_map_tab.id_map%TYPE ) return varchar2 is
  
       v_id_tarefa gsf_atr_map_tab.id_map%TYPE;
       v_status    gsf_tarefa_tab.status%TYPE;
       
       begin
         
         v_id_tarefa := p_id_tarefa;
         
         SELECT STATUS
           INTO v_status
           FROM GSF_TAREFA_TAB
          WHERE ID_TAREFA = v_id_tarefa; 
          
         return v_status; 
  
end obtem_status;

END GSF_TAREFA_PKG;
/
