create or replace view gsf_log as
select ID_LOG
     , COD_TIPO
     , GSF_TIPO_LOG_PKG.Obtem_Descricao(COD_TIPO) DESCRICAO_TIPO_LOG
     , TEXTO_LOG
     , VERSAO_LINHA
     , ID_OBJETO
     , DS_METODO
 from GSF_LOG_TAB
WITH READ ONLY;
