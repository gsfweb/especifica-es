CREATE OR REPLACE VIEW GSF_REGRA_LIV_FISCAL_P2 AS
SELECT id_regra
     , id_empresa
     , uf
     , cfop
     , cst
     , tipo
     , base_calculo
     , aliquota
     , imposto
     , valor
     , isenta_nao_tributada
     , outras
  FROM GSF_REGRA_LIV_FISCAL_P2_TAB
  WITH READ ONLY;
