create or replace package gsf_regra_liv_fiscal_p2_pkg is

TYPE   rec_regra IS RECORD (
       base_calculo           VARCHAR2(1)
     , aliquota               VARCHAR2(1)
     , imposto                VARCHAR2(1)
     , valor                  VARCHAR2(1)
     , isenta_nao_tributada   VARCHAR2(1)
     , outras                 VARCHAR2(1));

TYPE t_regra IS VARRAY(1) OF rec_regra;


procedure INSERIR( p_id_empresa               gsf_regra_liv_fiscal_p2_tab.id_empresa%TYPE
                 , p_uf                       gsf_regra_liv_fiscal_p2_tab.uf%TYPE
                 , p_cfop                     gsf_regra_liv_fiscal_p2_tab.cfop%TYPE
                 , p_cst                      gsf_regra_liv_fiscal_p2_tab.cst%TYPE
                 , p_tipo                     gsf_regra_liv_fiscal_p2_tab.tipo%TYPE
                 , p_base_calculo             gsf_regra_liv_fiscal_p2_tab.base_calculo%TYPE
                 , p_aliquota                 gsf_regra_liv_fiscal_p2_tab.aliquota%TYPE
                 , p_imposto                  gsf_regra_liv_fiscal_p2_tab.imposto%TYPE
                 , p_valor                    gsf_regra_liv_fiscal_p2_tab.valor%TYPE
                 , p_isenta_nao_tributada     gsf_regra_liv_fiscal_p2_tab.isenta_nao_tributada%TYPE
                 , p_outras                   gsf_regra_liv_fiscal_p2_tab.outras%TYPE);
                 

procedure ALTERAR( p_id_regra                 gsf_regra_liv_fiscal_p2_tab.id_regra%TYPE
                 , p_id_empresa               gsf_regra_liv_fiscal_p2_tab.id_empresa%TYPE
                 , p_uf                       gsf_regra_liv_fiscal_p2_tab.uf%TYPE
                 , p_cfop                     gsf_regra_liv_fiscal_p2_tab.cfop%TYPE
                 , p_cst                      gsf_regra_liv_fiscal_p2_tab.cst%TYPE
                 , p_tipo                     gsf_regra_liv_fiscal_p2_tab.tipo%TYPE
                 , p_base_calculo             gsf_regra_liv_fiscal_p2_tab.base_calculo%TYPE
                 , p_aliquota                 gsf_regra_liv_fiscal_p2_tab.aliquota%TYPE
                 , p_imposto                  gsf_regra_liv_fiscal_p2_tab.imposto%TYPE
                 , p_valor                    gsf_regra_liv_fiscal_p2_tab.valor%TYPE
                 , p_isenta_nao_tributada     gsf_regra_liv_fiscal_p2_tab.isenta_nao_tributada%TYPE
                 , p_outras                   gsf_regra_liv_fiscal_p2_tab.outras%TYPE );                 
                 

procedure REMOVER( p_id_regra gsf_regra_liv_fiscal_p2_tab.id_regra%TYPE );

FUNCTION Obtem_Id_Regra (p_id_empresa NUMBER,
                         p_tipo       VARCHAR2, 
                         p_uf         VARCHAR2, 
                         p_cfop       VARCHAR2, 
                         p_cst        VARCHAR2) RETURN NUMBER;

FUNCTION Obtem_Dados_Regra (p_id_empresa NUMBER,
                            p_tipo       VARCHAR2,
                            p_uf         VARCHAR2, 
                            p_cfop       VARCHAR2,
                            p_cst        VARCHAR2) RETURN t_regra;


end;
/
create or replace package body gsf_regra_liv_fiscal_p2_pkg is

v_sql_dynamic clob;


PROCEDURE INSERIR( p_id_empresa               gsf_regra_liv_fiscal_p2_tab.id_empresa%TYPE
                 , p_uf                       gsf_regra_liv_fiscal_p2_tab.uf%TYPE
                 , p_cfop                     gsf_regra_liv_fiscal_p2_tab.cfop%TYPE
                 , p_cst                      gsf_regra_liv_fiscal_p2_tab.cst%TYPE
                 , p_tipo                     gsf_regra_liv_fiscal_p2_tab.tipo%TYPE
                 , p_base_calculo             gsf_regra_liv_fiscal_p2_tab.base_calculo%TYPE
                 , p_aliquota                 gsf_regra_liv_fiscal_p2_tab.aliquota%TYPE
                 , p_imposto                  gsf_regra_liv_fiscal_p2_tab.imposto%TYPE
                 , p_valor                    gsf_regra_liv_fiscal_p2_tab.valor%TYPE
                 , p_isenta_nao_tributada     gsf_regra_liv_fiscal_p2_tab.isenta_nao_tributada%TYPE
                 , p_outras                   gsf_regra_liv_fiscal_p2_tab.outras%TYPE) IS
                 

       v_id_regra                 gsf_regra_liv_fiscal_p2_tab.id_regra%TYPE;
       v_id_empresa               gsf_regra_liv_fiscal_p2_tab.id_empresa%TYPE;
       v_uf                       gsf_regra_liv_fiscal_p2_tab.uf%TYPE;
       v_cfop                     gsf_regra_liv_fiscal_p2_tab.cfop%TYPE;
       v_cst                      gsf_regra_liv_fiscal_p2_tab.cst%TYPE;
       v_tipo                     gsf_regra_liv_fiscal_p2_tab.tipo%TYPE;
       v_base_calculo             gsf_regra_liv_fiscal_p2_tab.base_calculo%TYPE;
       v_aliquota                 gsf_regra_liv_fiscal_p2_tab.aliquota%TYPE;
       v_imposto                  gsf_regra_liv_fiscal_p2_tab.imposto%TYPE;
       v_valor                    gsf_regra_liv_fiscal_p2_tab.valor%TYPE;
       v_isenta_nao_tributada     gsf_regra_liv_fiscal_p2_tab.isenta_nao_tributada%TYPE;
       v_outras                   gsf_regra_liv_fiscal_p2_tab.outras%TYPE;
       
       BEGIN
         
         v_id_empresa               := p_id_empresa;
         v_uf                       := p_uf;
         v_cfop                     := p_cfop;
         v_cst                      := p_cst;
         v_tipo                     := p_tipo;
         v_base_calculo             := p_base_calculo;
         v_aliquota               	:= p_aliquota;
         v_imposto                  := p_imposto;
         v_valor                    := p_valor;
         v_isenta_nao_tributada     := p_isenta_nao_tributada;
         v_outras                   := p_outras;
         
         SELECT gsf_liv_fiscal_regra_p2_seq.nextval 
           INTO v_id_regra 
           FROM dual;
         
         v_sql_dynamic := 'INSERT INTO gsf_regra_liv_fiscal_p2_tab( ID_REGRA
                                                    , ID_EMPRESA
                                                    , UF
                                                    , CFOP
                                                    , CST
                                                    , TIPO
                                                    , BASE_CALCULO
                                                    , ALIQUOTA
                                                    , imposto
                                                    , valor
                                                    , isenta_nao_tributada
                                                    , outras )
                                             VALUES ( :id_regra
                                                    , :id_empresa
                                                    , :uf
                                                    , :cfop
                                                    , :cst
                                                    , :tipo
                                                    , :base_calculo
                                                    , :aliquota
                                                    , :imposto
                                                    , :valor
                                                    , :isenta_nao_tributada
                                                    , :outras )';
         
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_id_regra
             , v_id_empresa
             , v_uf
             , v_cfop
             , v_cst
             , v_tipo
             , v_base_calculo
             , v_aliquota
             , v_imposto
             , v_valor
             , v_isenta_nao_tributada
             , v_outras;
             
         COMMIT;
         
         EXCEPTION
           WHEN OTHERS THEN
             ROLLBACK;
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'gsf_liv_fiscal_regra_ipi_PKG.INSERIR');
             dbms_output.put_line(SQLERRM);    
         

END INSERIR;

PROCEDURE ALTERAR( p_id_regra                 gsf_regra_liv_fiscal_p2_tab.id_regra%TYPE
                 , p_id_empresa               gsf_regra_liv_fiscal_p2_tab.id_empresa%TYPE
                 , p_uf                       gsf_regra_liv_fiscal_p2_tab.uf%TYPE
                 , p_cfop                     gsf_regra_liv_fiscal_p2_tab.cfop%TYPE
                 , p_cst                      gsf_regra_liv_fiscal_p2_tab.cst%TYPE
                 , p_tipo                     gsf_regra_liv_fiscal_p2_tab.tipo%TYPE
                 , p_base_calculo             gsf_regra_liv_fiscal_p2_tab.base_calculo%TYPE
                 , p_aliquota                 gsf_regra_liv_fiscal_p2_tab.aliquota%TYPE
                 , p_imposto                  gsf_regra_liv_fiscal_p2_tab.imposto%TYPE
                 , p_valor                    gsf_regra_liv_fiscal_p2_tab.valor%TYPE
                 , p_isenta_nao_tributada     gsf_regra_liv_fiscal_p2_tab.isenta_nao_tributada%TYPE
                 , p_outras                   gsf_regra_liv_fiscal_p2_tab.outras%TYPE ) IS

       v_id_regra                 gsf_regra_liv_fiscal_p2_tab.id_regra%TYPE;
       v_id_empresa               gsf_regra_liv_fiscal_p2_tab.id_empresa%TYPE;
       v_uf                       gsf_regra_liv_fiscal_p2_tab.uf%TYPE;
       v_cfop                     gsf_regra_liv_fiscal_p2_tab.cfop%TYPE;
       v_cst                      gsf_regra_liv_fiscal_p2_tab.cst%TYPE;
       V_tipo                     gsf_regra_liv_fiscal_p2_tab.tipo%TYPE;
       v_base_calculo             gsf_regra_liv_fiscal_p2_tab.base_calculo%TYPE;
       v_aliquota                 gsf_regra_liv_fiscal_p2_tab.aliquota%TYPE;
       v_imposto                  gsf_regra_liv_fiscal_p2_tab.imposto%TYPE;
       v_valor                    gsf_regra_liv_fiscal_p2_tab.valor%TYPE;
       v_isenta_nao_tributada     gsf_regra_liv_fiscal_p2_tab.isenta_nao_tributada%TYPE;
       v_outras                   gsf_regra_liv_fiscal_p2_tab.outras%TYPE;
       
       BEGIN
         
         v_id_regra 		            := p_id_regra;
         v_id_empresa               := p_id_empresa;
         v_uf                       := p_uf;
         v_cfop                     := p_cfop;
         v_cst                      := p_cst;
         v_tipo                     := p_tipo;
         v_base_calculo             := p_base_calculo;
         v_aliquota               	:= p_aliquota;
         v_imposto                  := p_imposto;
         v_valor                    := p_valor;
         v_isenta_nao_tributada     := p_isenta_nao_tributada;
         v_outras                   := p_outras;
         
         v_sql_dynamic := 'UPDATE gsf_regra_liv_fiscal_p2_tab
                              SET ID_EMPRESA               = :id_empresa
                                , UF                       = :uf
                                , CFOP                     = :cfop
                                , CST                      = :cst
                                , TIPO                     = :tipo
                                , BASE_CALCULO             = :base_calculo
                                , ALIQUOTA                 = :aliquota
                                , imposto                  = :imposto
                                , valor                    = :valor
                                , isenta_nao_tributada     = :isenta_nao_tributada
                                , outras                   = :outras
                            WHERE ID_REGRA = :id_regra';
         
         
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_id_empresa
             , v_uf
             , v_cfop
             , v_cst
             , v_tipo
             , v_base_calculo
             , v_aliquota
             , v_imposto
             , v_valor
             , v_isenta_nao_tributada
             , v_outras
             , v_id_regra;
         
         COMMIT;
         
         EXCEPTION
           WHEN OTHERS THEN
             ROLLBACK;
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'gsf_liv_fiscal_regra_ipi_PKG.ALTERAR');
             dbms_output.put_line(SQLERRM);
                      

END ALTERAR;

PROCEDURE REMOVER( p_id_regra gsf_regra_liv_fiscal_p2_tab.id_regra%TYPE ) IS
  
       v_id_regra gsf_regra_liv_fiscal_p2_tab.id_regra%TYPE;
       
       BEGIN
         
         v_id_regra := p_id_regra;
         
         v_sql_dynamic := 'DELETE 
                             FROM gsf_regra_liv_fiscal_p2_tab
                            WHERE ID_REGRA = :id_regra';
         
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_id_regra;
         
         COMMIT;
         
         EXCEPTION
           WHEN OTHERS THEN
             ROLLBACK;
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'gsf_liv_fiscal_regra_ipi_PKG.REMOVER');
             dbms_output.put_line(SQLERRM);
  
END REMOVER;

FUNCTION Obtem_Id_Regra (p_id_empresa NUMBER,
                         p_tipo       VARCHAR2, 
                         p_uf         VARCHAR2, 
                         p_cfop       VARCHAR2, 
                         p_cst        VARCHAR2) RETURN NUMBER
IS
	v_temp NUMBER;
BEGIN	
  SELECT id_regra
  INTO   v_temp
  FROM	 gsf_regra_liv_fiscal_p2_tab
  WHERE  id_empresa       = p_id_empresa
  AND    NVL(p_tipo, ' ') = NVL(p_tipo, ' ')
  AND	   NVL(uf, ' ')     = NVL(p_uf, ' ')
  AND	   NVL(cfop, ' ')   = NVL(p_cfop, ' ')
  AND	   NVL(cst, ' ')    = NVL(p_cst, ' ');

	RETURN v_temp;
END;

FUNCTION Obtem_Dados_Regra (p_id_empresa NUMBER,
                            p_tipo       VARCHAR2,
                            p_uf         VARCHAR2, 
                            p_cfop       VARCHAR2,
                            p_cst        VARCHAR2) RETURN t_regra
IS	
	t_array t_regra;
BEGIN	
  SELECT base_calculo,
         aliquota,
         imposto,
         valor,
         isenta_nao_tributada,
         outras
  BULK COLLECT INTO t_array
	FROM	 gsf_regra_liv_fiscal_p2_tab
  WHERE  id_empresa       = p_id_empresa
    AND  NVL(p_tipo, ' ') = NVL(p_tipo, ' ')
    AND	 NVL(uf, ' ')     = NVL(p_uf, ' ')
    AND	 NVL(cfop, ' ')   = NVL(p_cfop, ' ')
    AND	 NVL(cst, ' ')    = NVL(p_cst, ' ');

	RETURN t_array;
END;

end;
/
