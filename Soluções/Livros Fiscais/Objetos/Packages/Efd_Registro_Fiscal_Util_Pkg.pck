CREATE OR REPLACE PACKAGE Efd_Registro_Fiscal_Util_Pkg
IS
PROCEDURE Importar_Dados_Doc_Fiscal (
	  p_cod_part       IN VARCHAR2
	, p_periodo_fiscal IN VARCHAR2 );
  
FUNCTION busca_registro_fiscal (p_doc_fis    efd_registro_fiscal_tab.id_nota_fiscal%TYPE
                              , p_id_empresa efd_registro_fiscal_tab.id_empresa%TYPE) return number;  
  
END;
/
CREATE OR REPLACE PACKAGE BODY Efd_Registro_Fiscal_Util_Pkg
IS
PROCEDURE Importar_Dados_Doc_Fiscal (
	  p_cod_part     IN VARCHAR2
	, p_periodo_fiscal IN VARCHAR2 )
IS

  v_id_empresa  NUMBER ;
  v_data_inicio DATE;
  v_data_fim    DATE;
  
	CURSOR c_doc_fis IS
		SELECT a.*
		  FROM int_capa_docfis a
		 WHERE a.id_empresa = v_id_empresa
		   AND (a.data_emissao >= v_data_inicio
            AND a.data_emissao <= v_data_fim)
        OR (a.data_fiscal >= v_data_inicio
            AND a.data_fiscal <= v_data_fim);

  CURSOR c_item_fis(p_id_docfis IN NUMBER) IS
		SELECT a.*
		  FROM int_item_docfis a
		 WHERE a.id_docfis = p_id_docfis
     ORDER BY a.num_item;

	  t_reg_fis  EFD_REGISTRO_FISCAL_TAB%ROWTYPE;

	  t_item_fis EFD_ITEM_REGISTRO_FISCAL_TAB%ROWTYPE;

	  v_no_linha NUMBER;
    v_count    NUMBER;

BEGIN
  
  v_data_inicio := TO_DATE('01/'||p_periodo_fiscal, 'DD/MM/YYYY'); 
  v_data_fim    := LAST_DAY(v_data_inicio);
  v_id_empresa  := Gsf_Empresa_Pkg.Obtem_id_empresa(p_cod_part);
  
  FOR reg_doc_fis IN c_doc_fis LOOP
		v_no_linha := 0;
    
    t_reg_fis.id_nota_fiscal                := reg_doc_fis.id_docfis;
    t_reg_fis.no_nota_fiscal                := reg_doc_fis.num_docfis;
    t_reg_fis.serie                         := reg_doc_fis.serie_docfis;
    t_reg_fis.sub_serie                     := reg_doc_fis.sub_serie_docfis;
    t_reg_fis.uf_orig                       := reg_doc_fis.cod_estado_orig;
    t_reg_fis.endereco_dest                 := NULL; --reg_doc_fis.cod_dest_dest; 
    t_reg_fis.cidade_dest                   := NULL; --reg_doc_fis.cod_dest_dest_local;
    t_reg_fis.pais_dest                     := NULL;
    t_reg_fis.bairro_dest                   := NULL;
    t_reg_fis.uf_dest                       := reg_doc_fis.cod_estado_dest;
    t_reg_fis.cep_dest                      := NULL;
    t_reg_fis.cnpj_cpf_dest                 := reg_doc_fis.cod_sit;
    t_reg_fis.data_entrada                  := reg_doc_fis.data_fiscal;
    t_reg_fis.data_emissao                  := reg_doc_fis.data_emissao; --reg_doc_fis.data_saida_rec
    t_reg_fis.tipo_entrada_saida            := reg_doc_fis.movto_e_s;
    t_reg_fis.especie_nf                    := 'NF';
    t_reg_fis.no_documento                  := '55';
    t_reg_fis.value_frete                   := reg_doc_fis.vl_frete;
    t_reg_fis.valor_seguro                  := reg_doc_fis.vl_seg;
    t_reg_fis.valor_despesas_acessorias     := NULL;
    t_reg_fis.status_nota_fiscal            := NULL;
    t_reg_fis.valor_mercadoria              := reg_doc_fis.vl_merc;
    t_reg_fis.valor_ipi                     := reg_doc_fis.vl_ipi;
    t_reg_fis.valor_desconto                := reg_doc_fis.vl_desc;
    t_reg_fis.valor_substituicao_tributaria := NULL;
		t_reg_fis.inscricao_substituicao_trib   := NULL;
		t_reg_fis.quantidade_volume             := NULL;
		t_reg_fis.especie_volume                := NULL;
		t_reg_fis.peso_bruto                    := NULL;
		t_reg_fis.peso_liquido                  := NULL;
		t_reg_fis.placa_veiculo                 := NULL;
		t_reg_fis.observa��o                    := NULL;
		t_reg_fis.tipo_nota_fiscal              := NULL;
		t_reg_fis.inscricao_suframa             := NULL;
		t_reg_fis.uf_veiculo                    := NULL;
    t_reg_fis.id_empresa                    := v_id_empresa;

		SELECT DECODE(reg_doc_fis.cod_sit, 'C', 'S', 'N') INTO t_reg_fis.cancelado FROM DUAL;
    
    
    efd_registro_fiscal_pkg.ALTERAR( reg_doc_fis.id_docfis, t_reg_fis.id_empresa, t_reg_fis, v_count );
    
    --registro inexistente, cria novo registro
    IF (v_count = 0) THEN

		  SELECT EFD_Registro_Fiscal_Seq.Nextval INTO t_reg_fis.id_reg_fiscal FROM DUAL;
    
      efd_registro_fiscal_pkg.Inserir(t_reg_fis);
    
    END IF;
    
    
    --itens
    FOR reg_item_fis IN c_item_fis(reg_doc_fis.id_docfis) LOOP
      
      --registro j� existe, procura registro_fiscal
      IF( t_reg_fis.id_reg_fiscal IS NULL ) THEN
      
        t_item_fis.id_reg_fiscal := busca_registro_fiscal(t_reg_fis.id_nota_fiscal
                                                        , t_reg_fis.id_empresa);    
        
      --registro recente, n�o possui na base de dados
      ELSE  
    
        t_item_fis.id_reg_fiscal := t_reg_fis.id_reg_fiscal;
      
      END IF;
      
      
      v_no_linha                            := v_no_linha + 1;        
      t_item_fis.no_linha                   := v_no_linha;

      t_item_fis.numero_item_nf             := reg_item_fis.num_item;
      t_item_fis.cod_produto                := reg_item_fis.cod_produto;
      t_item_fis.descricao_produto          := reg_item_fis.descr_compl;
      t_item_fis.Cfop                       := reg_item_fis.Cfop;
      t_item_fis.valor_desconto             := reg_item_fis.vl_desc;

      t_item_fis.quantidade_item            := reg_item_fis.qtd;
      t_item_fis.valor_total_item           := reg_item_fis.vl_item;
      
      t_item_fis.id_natureza_operacao       := NULL;
      t_item_fis.sub_item_natureza_operacao := NULL;
      
      t_item_fis.unidade_medida             := reg_item_fis.unid;
      t_item_fis.valor_despesa_acessoria    := NULL;
      t_item_fis.valor_contabil             := NULL;
      t_item_fis.valor_frete                := reg_item_fis.vl_frete;
      t_item_fis.perc_desconto              := NULL;

      t_item_fis.valor_seguro               := reg_item_fis.vl_seg;
      t_item_fis.valor_material             := NULL;
      t_item_fis.valor_frete_nota           := NULL;
      t_item_fis.texto_nota                 := NULL;
      t_item_fis.valor_servico              := NULL;
      t_item_fis.preco_bruto_unitario       := NULL;
      t_item_fis.preco_unitario             := NULL;
      t_item_fis.valor_custo_frete          := NULL;

      t_item_fis.cod_contribuicao_social    := NULL;
      t_item_fis.cod_tipo_credito           := NULL;
      t_item_fis.cod_base_credito           := NULL;
      t_item_fis.observacao                 := NULL;

      --CSLL
      t_item_fis.valor_csll_a_reter         := NULL;
      t_item_fis.perc_retencao_csll         := NULL;

      --IR
      t_item_fis.cod_retencao_irpj          := NULL;
      t_item_fis.valor_base_ir              := NULL;
      t_item_fis.perc_ir                    := NULL;
      t_item_fis.perc_reducao_ir            := NULL;
      t_item_fis.valor_ir                   := NULL;
      t_item_fis.valor_reter_ir             := NULL;

      -- ISS
      t_item_fis.perc_iss                   := NULL;
      t_item_fis.base_isenta_iss            := NULL;
      t_item_fis.base_calculo_iss           := reg_item_fis.vl_bc_iss;
      t_item_fis.valor_iss                  := reg_item_fis.vl_iss;
      t_item_fis.perc_reducao_iss           := NULL;
      t_item_fis.cod_servico_iss            := NULL;
      t_item_fis.valor_iss_a_reter          := NULL;
      t_item_fis.desc_cod_servico_iss       := NULL;

      -- INSS
      t_item_fis.cod_retencao_inss          := NULL;
      t_item_fis.perc_inss                  := NULL;
      t_item_fis.perc_reducao_inss          := NULL;
      t_item_fis.valor_inss                 := NULL;
      t_item_fis.valor_inss_a_reter         := NULL;
      t_item_fis.base_calculo_inss          := NULL;

      -- ICMS
      t_item_fis.base_calculo_icms          := reg_item_fis.vl_bc_icms;
      t_item_fis.valor_icms                 := reg_item_fis.vl_icms;
      t_item_fis.perc_icms                  := reg_item_fis.aliq_icms;
      t_item_fis.tipo_operacao_icms         := SUBSTR(reg_item_fis.cfop, 1, 1);
      t_item_fis.cod_tributacao_icms        := SUBSTR(reg_item_fis.cfop, 3);
      t_item_fis.base_substituicao_icms     := reg_item_fis.Vl_Bc_Icms_St;
      t_item_fis.valor_substituicao_icms    := reg_item_fis.Vl_Icms_St;
      t_item_fis.cst_ICMS                   := reg_item_fis.Cst_Icms;

      -- n�o h� no momento (IN�CIO)
      /*IF reg_doc_fis.movto_e_s = '1' THEN
        IF (reg_item_fis.tipo_operacao_icms = '1') AND (reg_item_fis.classificacao_icms = '2') THEN
          t_item_fis.outras_bases_icms := reg_item_fis.vl_bc_icms;
          t_item_fis.base_calculo_icms := 0;
          t_item_fis.valor_icms := 0;
          t_item_fis.perc_icms := 0;
          t_item_fis.tipo_operacao_icms := '5'; -- Outras
          t_item_fis.cod_tributacao_icms := '90';
          t_item_fis.base_substituicao_icms := 0;
          t_item_fis.valor_substituicao_icms := 0;
        END IF;
      END IF;*/
      -- n�o h� no momento (FIM)

      /*IF reg_item_fis.classificacao_icms NOT IN ('1', '2') THEN
          t_item_fis.classificacao_icms := NULL;
          -- Iserir na tabela de logs
          -- classifica��o de icms n�o cadastrada
        END IF;*/

      -- criar verifica��o para exist�ncia de c�digo de c�lculo ICMS
      --BEGIN
        --t_item_fis.cod_calculo_icms
      --EXCEPTION
      --  WHEN OTHERS THEN
        -- Inserir na tabela de logs
        --Log_Pkg.Inserir(...);
      --END;

      t_item_fis.perc_base_reducao_icms     := NULL;
      t_item_fis.classificacao_icms         := NULL;
      t_item_fis.base_insenta_icms          := NULL;
      t_item_fis.valor_diferenca_icms       := NULL;
      t_item_fis.perc_icms_zone_franca      := NULL;
      t_item_fis.valor_icms_zona_franca     := NULL;
      t_item_fis.valor_retencao_icms        := NULL;
      t_item_fis.valor_parcial_antecip_icms := NULL;

      IF (t_item_fis.valor_substituicao_icms > 0) THEN
        t_item_fis.substituicao_icms        := 'S';
      ELSE
        t_item_fis.substituicao_icms        := 'N';
      END IF;

      -- n�o h� no momento (IN�CIO)
      -- IPI
      /*IF reg_doc_fis.movto_e_s = '1' THEN -- 1-Entrada, 2-Sa�da
        IF (reg_item_fis.tipo_calculo_ipi = '1') AND (reg_item_fis.classificacao_ipi = '2') THEN
          t_item_fis.outras_bases_ipi := reg_item_fis.vl_bc_ipi;
          t_item_fis.base_calculo_ipi := 0;
          t_item_fis.valor_ipi := 0;
          t_item_fis.perc_ipi := 0;
          t_item_fis.tipo_operacao_ipi := '4'; -- Outras
        END IF;
      END IF;*/
      -- n�o h� no momento (FIM)

      t_item_fis.perc_base_reducao_ipi := NULL;
      t_item_fis.classificacao_ipi     := NULL;
      t_item_fis.base_isenta_ipi       := NULL;
      t_item_fis.perc_recuperacao_ipi  := NULL;
      t_item_fis.valor_ipi_a_recuperar := NULL;
      t_item_fis.valor_ipi_descolado   := NULL;
      t_item_fis.valor_pis             := reg_item_fis.aliq_ipi;
      t_item_fis.Valor_Ipi             := reg_item_fis.vl_ipi;
      t_item_fis.cst_IPI               := reg_item_fis.Cst_IPI;

      /*IF (reg_item_fis.tipo_calculo_ipi = '1') AND (reg_item_fis.ipi_percentage = 0) THEN
        t_item_fis.outras_bases_ipi := reg_item_fis.outras_bases_ipi;
        t_item_fis.base_calculo_ipi := 0;
      END IF;*/

      -- PIS
      /*IF reg_doc_fis.movto_e_s = '1' THEN -- 1-Entrada, 2-Sa�da
        IF (reg_item_fis.classificacao_pis = '2') THEN
          t_item_fis.base_calculo_pis := 0;
          t_item_fis.valor_pis := 0;
          t_item_fis.perc_pis := 0;
        ELSE
          t_item_fis.base_calculo_pis := reg_item_fis.base_calculo_pis;
        END IF;
      ELSE
        t_item_fis.base_calculo_pis := reg_item_fis.base_calculo_pis;
      END IF;*/

      t_item_fis.valor_pis_a_reter     := NULL;
      t_item_fis.perc_pis_zona_franca  := NULL;
      t_item_fis.valor_pis_zona_franca := NULL;
      t_item_fis.perc_retencao_pis     := NULL;
      t_item_fis.cod_retencao_pis      := NULL;
      t_item_fis.linha_retencao_pis    := NULL;
      t_item_fis.perc_base_reducao_pis := NULL;
      t_item_fis.cst_PIS               := reg_item_fis.Cst_Pis;

      -- COFINS
      /*IF reg_doc_fis.movto_e_s = '1' THEN -- 1-Entrada, 2-Sa�da
        IF (reg_item_fis.classificacao_cofins = '2') THEN
          t_item_fis.base_calculo_cofins := 0;
          t_item_fis.valor_cofins := 0;
          t_item_fis.perc_cofins := 0;
        ELSE
          t_item_fis.base_calculo_cofins := ?;
        END IF;
      ELSE
        t_item_fis.base_calculo_cofins := ?;
      END IF;*/

      t_item_fis.valor_cofins             := NULL;
      t_item_fis.perc_cofins              := NULL;
      t_item_fis.valor_cofins_a_reter     := NULL;
      t_item_fis.perc_cofins_zona_franca  := NULL;
      t_item_fis.valor_cofins_zona_franca := NULL;
      t_item_fis.perc_retencao_cofins     := NULL;
      t_item_fis.cod_retencao_cofins      := NULL;
      t_item_fis.linha_retencao_cofins    := NULL;
      t_item_fis.perc_base_reducao_cofins := NULL;
      t_item_fis.cst_cofins               := reg_item_fis.Cst_Cofins;

      -- Criar verifica��o se classifica��o ipi existe
      --BEGIN
        --reg_item_fis.classificacao_ipi
      --EXCEPTION
      --  WHEN OTHERS THEN
        -- gravar na tabela de logs
      --END;

      -- Criar verifica��o se tipo de opera��o ipi existe
      --BEGIN
        --reg_item_fis.tipo_operacao_ipi
      --EXCEPTION
      --  WHEN OTHERS THEN
          -- Inserir na tabela de logs que tipo_calculo_ipi n�o existe
      --END;

      -- Criar verifica��o para c�digo de servi�o ISS
      --BEGIN
        --reg_item_fis.cod_servico_iss
      --EXCEPTION
      --  WHEN OTHERS THEN
        -- Inserir na tabela de logs que cod_servico_iss n�o existe
      --END;

      --IF p_sobrepor = 'V' THEN
           -- Criar instru��o para sobrepor registro anterior
      --     NULL;
      --END IF;
      
      t_item_fis.cod_enquadramento := reg_item_fis.cod_enq;
      
      efd_item_registro_fiscal_pkg.ALTERAR(t_item_fis.id_reg_fiscal
                                         , t_item_fis.no_linha
                                         , t_item_fis
                                         , v_count );
      
      IF(v_count = 0) THEN
      
        efd_item_registro_fiscal_pkg.INSERIR(t_item_fis);
      
      END IF;  
    
    END LOOP;        
   
  END LOOP;
END;



FUNCTION busca_registro_fiscal (p_doc_fis    efd_registro_fiscal_tab.id_nota_fiscal%TYPE
                              , p_id_empresa efd_registro_fiscal_tab.id_empresa%TYPE) return number is
  
       v_doc_fis    efd_registro_fiscal_tab.id_nota_fiscal%TYPE;
       v_id_empresa efd_registro_fiscal_tab.id_empresa%TYPE;
       v_reg_fiscal efd_registro_fiscal_tab.id_reg_fiscal%TYPE;
       
       BEGIN
          
         v_doc_fis    := p_doc_fis;
         v_id_empresa := p_id_empresa;
        
         SELECT EFT.ID_REG_FISCAL
           INTO v_reg_fiscal   
           FROM EFD_REGISTRO_FISCAL_TAB EFT
          WHERE EFT.ID_EMPRESA     = v_id_empresa
            AND EFT.ID_NOTA_FISCAL = v_doc_fis;
         
         RETURN v_reg_fiscal;   

end busca_registro_fiscal; 


END;
/
