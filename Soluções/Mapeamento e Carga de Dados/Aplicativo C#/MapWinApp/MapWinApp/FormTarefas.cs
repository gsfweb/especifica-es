﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
//using System.Data.OleDb;
using Oracle.DataAccess.Client;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MapWinApp
{
    public partial class FormTarefas : Form
    {
        public FormSelecionarMapeamento frmS = new FormSelecionarMapeamento();

        public FormTarefas()
        {
            InitializeComponent();
        }

        private void textBoxIdMapeamento_TextChanged(object sender, EventArgs e)
        {

        }

        private bool ExecutarCargaDadosTarefa(bool is_checked, string id_tarefa)
        {
            if (is_checked == true)
            {
                {
                    OracleParameter pCarga = new OracleParameter("id_tarefa", OracleDbType.Int32);
                    pCarga.Value = id_tarefa;
                    pCarga.Direction = ParameterDirection.Input;

                    OracleCommand cmdCarga = new OracleCommand();

                    cmdCarga.Connection = Database.connection;
                    cmdCarga.CommandText = "Gsf_Importacao_Pkg.Executar_Carga_Dados_Tarefa";
                    cmdCarga.CommandType = CommandType.StoredProcedure;

                    cmdCarga.Parameters.Add(pCarga);

                    try
                    {
                        cmdCarga.ExecuteNonQuery();
                        return true;
                    }
                    catch (OracleException ex)
                    {
                        MessageBox.Show("Ocorreu erro ao executar tarefa " + this.textBoxIdMapeamento.Text + ":" + Environment.NewLine + ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return false;
                    }
                }

            }

            return true;
        }

        private void checkBoxExecutar_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (this.Tag.ToString() == "NOVO")
            {
                if (this.textBoxIdMapeamento.Text.Trim() != string.Empty && this.textBoxDescricao.Text.Trim() != string.Empty)
                {
                    this.Cursor = Cursors.WaitCursor;

                    OracleCommand cmdErro = new OracleCommand();

                    cmdErro.Connection = Database.connection;
                    cmdErro.CommandText = "gsf_map_pkg.Existe";
                    cmdErro.CommandType = CommandType.StoredProcedure;

                    OracleParameter returnVal = new OracleParameter("RETURN_VALUE", null);
                    returnVal.Direction = ParameterDirection.ReturnValue;
                    returnVal.Size = 2000;
                    cmdErro.Parameters.Add(returnVal);

                    OracleParameter pId = new OracleParameter("p_id_map", OracleDbType.Int32);
                    pId.Direction = ParameterDirection.Input;
                    pId.Value = this.textBoxIdMapeamento.Text;
                    cmdErro.Parameters.Add(pId);

                    try
                    {
                        cmdErro.ExecuteNonQuery();

                        if (cmdErro.Parameters["RETURN_VALUE"].Value.ToString() == "VERDADEIRO")
                        {
                            OracleParameter p1 = new OracleParameter("p_id_map", OracleDbType.Int32);
                            p1.Value = this.textBoxIdMapeamento.Text.Trim();
                            p1.Direction = ParameterDirection.Input;

                            OracleParameter p2 = new OracleParameter("p_desc_tarefa", OracleDbType.Varchar2, 2000);
                            p2.Value = this.textBoxDescricao.Text.Trim();
                            p2.Direction = ParameterDirection.Input;

                            OracleParameter p3 = new OracleParameter("p_status", OracleDbType.Varchar2, 100);
                            p3.Direction = ParameterDirection.Output;

                            OracleParameter p4 = new OracleParameter("ret_id_tarefa", OracleDbType.Int32);
                            p4.Direction = ParameterDirection.Output;

                            OracleCommand cmd = new OracleCommand();

                            cmd.Connection = Database.connection;
                            cmd.CommandText = "GSF_TAREFA_PKG.inserir";
                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.Parameters.Add(p1);
                            cmd.Parameters.Add(p2);
                            cmd.Parameters.Add(p3);
                            cmd.Parameters.Add(p4);

                            try
                            {
                                cmd.ExecuteNonQuery();

                                this.textBoxIdTarefa.Text = cmd.Parameters["ret_id_tarefa"].Value.ToString();

                                if (ExecutarCargaDadosTarefa(this.checkBoxExecutar.Checked, this.textBoxIdTarefa.Text.Trim()))
                                {
                                    MessageBox.Show("Tarefa " + this.textBoxIdTarefa.Text + " criada e/ou carga efetuada.", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }

                                this.Cursor = Cursors.Default;

                                this.Close();
                            }
                            catch (OracleException ex)
                            {
                                this.Cursor = Cursors.Default;
                                MessageBox.Show("Ocorreu erro ao criar tarefa: " + ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {
                            this.Cursor = Cursors.Default;
                            MessageBox.Show("Não existe Id. Mapeamento informado.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                    catch (OracleException ex)
                    {
                        this.Cursor = Cursors.Default;
                        MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (this.textBoxIdTarefa.Text.ToString() == string.Empty && this.textBoxDescricao.Text.Trim() == string.Empty)
                {
                    this.Close();
                }
            }
            else if (this.Tag.ToString() == "ATUALIZAÇÃO")
            {
                if (this.textBoxIdTarefa.Text.Trim() != string.Empty && this.textBoxDescricao.Text.Trim() != string.Empty)
                {
                    this.Cursor = Cursors.WaitCursor;

                    OracleParameter p1 = new OracleParameter("p_id_tarefa", OracleDbType.Int32);
                    p1.Direction = ParameterDirection.Input;
                    p1.Value = this.textBoxIdTarefa.Text.Trim();

                    OracleParameter p2 = new OracleParameter("p_desc_tarefa", OracleDbType.Varchar2);
                    p2.Direction = ParameterDirection.Input;
                    p2.Value = this.textBoxDescricao.Text.Trim();

                    OracleCommand cmd = new OracleCommand();

                    cmd.Connection = Database.connection;
                    cmd.CommandText = "GSF_TAREFA_PKG.alterar";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(p1);
                    cmd.Parameters.Add(p2);

                    try
                    {
                        cmd.ExecuteNonQuery();

                        if (ExecutarCargaDadosTarefa(this.checkBoxExecutar.Checked, this.textBoxIdTarefa.Text.Trim()))
                        {
                            MessageBox.Show("Tarefa " + this.textBoxIdTarefa.Text + " alterada e/ou carga efetuada.", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Cursor = Cursors.Default;
                            this.Close();
                        }
                        else
                        {
                            this.Cursor = Cursors.Default;
                        }
                    }
                    catch (OracleException ex)
                    {
                        this.Cursor = Cursors.Default;
                        MessageBox.Show("Ocorreu erro ao alterar tarefa " + this.textBoxIdMapeamento.Text + ":" + ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                }
            }
            else if (this.Tag.ToString() == "EXCLUSÃO")
            {
                if (this.textBoxIdTarefa.Text.Trim() != string.Empty && this.textBoxIdMapeamento.Text.Trim() != string.Empty)
                {
                    this.Cursor = Cursors.WaitCursor;

                    OracleParameter p1 = new OracleParameter("id_tarefa", OracleDbType.Int32);
                    p1.Value = this.textBoxIdTarefa.Text;
                    p1.Direction = ParameterDirection.Input;


                    OracleCommand cmd = new OracleCommand();

                    cmd.Connection = Database.connection;
                    cmd.CommandText = "GSF_TAREFA_PKG.remover";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(p1);

                    try
                    {
                        cmd.ExecuteNonQuery();
                        this.Cursor = Cursors.Default;
                        this.Close();
                    }
                    catch (OracleException ex)
                    {
                        this.Cursor = Cursors.Default;
                        MessageBox.Show("Ocorreu erro ao excluir tarefa " + this.textBoxIdMapeamento.Text + ":" + Environment.NewLine + ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                }
            }

        }

        private void buttonPesquisaIdMap_Click(object sender, EventArgs e)
        {
            frmS.textBoxTextoPesquisa.Text = string.Empty;

            frmS.ShowDialog(this);
        }

        private void FormTarefas_Load(object sender, EventArgs e)
        {

        }

        private void FormTarefas_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }

        }
    }
}
