﻿using System.Windows.Forms;

namespace MapWinApp
{
    partial class FormTabelasDestino
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormTabelasDestino));
            this.buttonOK = new System.Windows.Forms.Button();
            this.comboBoxTabelasDestino = new System.Windows.Forms.ComboBox();
            this.labelCodTabela = new System.Windows.Forms.Label();
            this.textBoxDescricao = new System.Windows.Forms.TextBox();
            this.labelDescricao = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonOK
            // 
            this.buttonOK.Enabled = false;
            this.buttonOK.Location = new System.Drawing.Point(328, 220);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 8;
            this.buttonOK.Text = "OK";
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // comboBoxTabelasDestino
            // 
            this.comboBoxTabelasDestino.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTabelasDestino.FormattingEnabled = true;
            this.comboBoxTabelasDestino.Location = new System.Drawing.Point(15, 25);
            this.comboBoxTabelasDestino.Name = "comboBoxTabelasDestino";
            this.comboBoxTabelasDestino.Size = new System.Drawing.Size(250, 21);
            this.comboBoxTabelasDestino.TabIndex = 5;
            this.comboBoxTabelasDestino.DropDown += new System.EventHandler(this.comboBoxTabelasDestino_DropDown);
            this.comboBoxTabelasDestino.SelectedIndexChanged += new System.EventHandler(this.comboBoxTabelasDestino_SelectedIndexChanged);
            this.comboBoxTabelasDestino.SelectedValueChanged += new System.EventHandler(this.comboBoxTabelasDestino_SelectedValueChanged);
            // 
            // labelCodTabela
            // 
            this.labelCodTabela.AutoSize = true;
            this.labelCodTabela.Location = new System.Drawing.Point(12, 10);
            this.labelCodTabela.Name = "labelCodTabela";
            this.labelCodTabela.Size = new System.Drawing.Size(43, 13);
            this.labelCodTabela.TabIndex = 4;
            this.labelCodTabela.Text = "Tabela:";
            // 
            // textBoxDescricao
            // 
            this.textBoxDescricao.Location = new System.Drawing.Point(15, 77);
            this.textBoxDescricao.Name = "textBoxDescricao";
            this.textBoxDescricao.Size = new System.Drawing.Size(388, 20);
            this.textBoxDescricao.TabIndex = 7;
            this.textBoxDescricao.TextChanged += new System.EventHandler(this.comboBoxTabelasDestino_SelectedValueChanged);
            // 
            // labelDescricao
            // 
            this.labelDescricao.AutoSize = true;
            this.labelDescricao.Location = new System.Drawing.Point(12, 61);
            this.labelDescricao.Name = "labelDescricao";
            this.labelDescricao.Size = new System.Drawing.Size(58, 13);
            this.labelDescricao.TabIndex = 6;
            this.labelDescricao.Text = "Descrição:";
            // 
            // FormTabelasDestino
            // 
            this.AcceptButton = this.buttonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(411, 255);
            this.Controls.Add(this.labelDescricao);
            this.Controls.Add(this.textBoxDescricao);
            this.Controls.Add(this.comboBoxTabelasDestino);
            this.Controls.Add(this.labelCodTabela);
            this.Controls.Add(this.buttonOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormTabelasDestino";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Tabelas de Destino";
            this.Load += new System.EventHandler(this.FormTabelasDestino_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormTabelasDestino_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonOK;
        public ComboBox comboBoxTabelasDestino;
        private Label labelCodTabela;
        public TextBox textBoxDescricao;
        private Label labelDescricao;
    }
}