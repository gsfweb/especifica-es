﻿using System;
using System.Collections.Generic;
using System.Data;
using Oracle.DataAccess.Client;
using System.Text;
using System.Windows.Forms;

namespace MapWinApp
{
    public partial class FormConexao : Form
    {
        public bool conexaoOK;

        public FormConexao()
        {
            InitializeComponent();
        }

        private void FormConexao_Load(object sender, EventArgs e)
        {
            try
            {
                this.Tag = string.Empty;

                conexaoOK = false;

                if (MapWinApp.Properties.Settings.Default["OracleTNSName"].ToString() != string.Empty)
                {
                    this.textBoxTNSName.Text = MapWinApp.Properties.Settings.Default["OracleTNSName"].ToString();
                }

                if (MapWinApp.Properties.Settings.Default["OracleUsername"].ToString() != string.Empty)
                {
                    this.textBoxUserName.Text = MapWinApp.Properties.Settings.Default["OracleUsername"].ToString();
                }

                if (this.textBoxTNSName.Text != string.Empty && this.textBoxUserName.Text == string.Empty)
                {
                    this.ActiveControl = this.textBoxUserName;
                }
                else if (this.textBoxTNSName.Text != string.Empty & this.textBoxUserName.Text != string.Empty)
                {
                    this.ActiveControl = this.textBoxPassword;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (this.textBoxTNSName.Text.Trim() != null && this.textBoxUserName.Text.Trim() != null && this.textBoxPassword.Text.Trim() != null)
            {
                this.Cursor = Cursors.WaitCursor;

                Database.connection.ConnectionString = "Data Source=" + this.textBoxTNSName.Text.Trim() + ";User Id=" + this.textBoxUserName.Text.Trim() + ";Password=" + this.textBoxPassword.Text.Trim() + ";";

                try
                {
                    Database.connection.Open();

                    if (Database.connection.State == ConnectionState.Open)
                    {
                        this.conexaoOK = true;
                        this.Cursor = Cursors.Default;
                        this.Close();
                    }
                    else
                    {
                        this.Cursor = Cursors.Default;
                    }

            }
                catch (OracleException ex)
                {
                    MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void FormConexao_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }

        private void comboBoxTNSNames_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void labelUserName_Click(object sender, EventArgs e)
        {

        }

        private void labelPassword_Click(object sender, EventArgs e)
        {

        }

        private void textBoxPassword_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxUserName_TextChanged(object sender, EventArgs e)
        {

        }

        private void labelOracleHomes_Click(object sender, EventArgs e)
        {

        }

        private void FormConexao_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                Application.Exit();
            }
        }

        private void buttonConstrutorTNSName_Click(object sender, EventArgs e)
        {
            FormConstrutorTNS frmC = new FormConstrutorTNS();

            frmC.FormConstrutorTNS_Load(null, null);

            if (this.textBoxTNSName.Text.Trim() != string.Empty)
            {
                int inic;
                int fim;

                inic = this.textBoxTNSName.Text.IndexOf("PROTOCOL");
                fim = this.textBoxTNSName.Text.IndexOf(")", inic);
                string protocol = this.textBoxTNSName.Text.Substring(inic, fim - inic).Split('=')[1].Trim().ToLower();

                switch (protocol )
                {
                    case "ipc":
                        frmC.comboBoxProtocolo.SelectedIndex = 0;
                        break;
                    case "nmp":
                        frmC.comboBoxProtocolo.SelectedIndex = 1;
                        break;
                    case "sdp":
                        frmC.comboBoxProtocolo.SelectedIndex = 2;
                        break;
                    case "tcp":
                        frmC.comboBoxProtocolo.SelectedIndex = 3;
                        break;
                    case "tcps":
                        frmC.comboBoxProtocolo.SelectedIndex = 4;
                        break;
                }

                inic = this.textBoxTNSName.Text.IndexOf("HOST");
                fim = this.textBoxTNSName.Text.IndexOf(")", inic);
                string host = this.textBoxTNSName.Text.Substring(inic, fim - inic);

                frmC.textBoxHostServer.Text = host.Split('=')[1].Trim();

                inic = this.textBoxTNSName.Text.IndexOf("PORT");
                fim = this.textBoxTNSName.Text.IndexOf(")", inic);
                string port = this.textBoxTNSName.Text.Substring(inic, fim - inic);

                frmC.textBoxKeyPipePort.Text = port.Split('=')[1].Trim();

                inic = this.textBoxTNSName.Text.IndexOf("SERVICE_NAME");
                fim = this.textBoxTNSName.Text.IndexOf(")", inic);
                string service_name = this.textBoxTNSName.Text.Substring(inic, fim - inic);

                frmC.textBoxServiceName.Text = service_name.Split('=')[1].Trim();
            }

            frmC.ShowDialog(this);
        }

        private void FormConexao_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                MapWinApp.Properties.Settings.Default["OracleTNSName"] = this.textBoxTNSName.Text;
                MapWinApp.Properties.Settings.Default["OracleUsername"] = this.textBoxUserName.Text;
                MapWinApp.Properties.Settings.Default.Save();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void FormConexao_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            StringBuilder builder = new StringBuilder();

            //builder.Append("DLLs necessárias para execução deste aplicativo que devem estar no mesma pasta do EXE:").AppendLine().AppendLine();
            //builder.Append(" OraOps12.dll").AppendLine();
            //builder.Append(" Oracle.DataAccess.dll").AppendLine();
            //builder.Append(" oci.dll").AppendLine();
            //builder.Append(" ociw32.dll").AppendLine();
            //builder.Append(" orannzsbb12.dll").AppendLine();
            //builder.Append(" oraocci12.dll").AppendLine();
            //builder.Append(" oraociicus12.dll").AppendLine();
            //builder.Append(" oraons.dll").AppendLine().AppendLine();

            builder.Append(" 1) TNS Name: Cadeia de caracteres no formato: \"(DESCRIPTION = (ADDRESS = (PROTOCOL = ?)(HOST = ?)(PORT = ?)) (CONNECT_DATA = (SERVER = DEDICATED) (SERVICE_NAME = ?)))\" para conexão ao banco de dados Oracle.").AppendLine();
            builder.Append(" 2) Usuário: Nome do usuário do banco de dados Oracle.").AppendLine();
            builder.Append(" 3) Senha: Password do banco de dados Oracle.").AppendLine();

            MessageBox.Show(builder.ToString(), "Conexão ao Banco de Dados Oracle");
        }

        private void labelTNSName_Click(object sender, EventArgs e)
        {

        }
    }
}
