﻿using System;
using System.Data;
//using System.Data.OleDb;
using Oracle.DataAccess.Client;
using System.Windows.Forms;
using System.Linq;

namespace MapWinApp
{
    public partial class FormTabelasOrigem : Form
    {
        public FormTabelasOrigem()
        {
            InitializeComponent();
        }

        private void FormTabelasOrigem_Load(object sender, EventArgs e)
        {
            //this.
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (this.Tag.ToString() != "EXCLUSÃO")
            { 
                if (this.comboBoxEmpresa.Text != null && this.comboBoxTabelasOrigem.Text != null && this.textBoxDescricao.Text.Trim() != null)
                {
                    this.Cursor = Cursors.WaitCursor;

                    OracleCommand cmd1 = new OracleCommand();

                    cmd1.Connection = Database.connection;
                    cmd1.CommandText = "gsf_tabela_orig_pkg.Obtem_Descricao";
                    cmd1.CommandType = CommandType.StoredProcedure;

                    OracleParameter returnVal = new OracleParameter("RETURN_VALUE", null);
                    returnVal.Direction = ParameterDirection.ReturnValue;
                    returnVal.Size = 2000;
                    cmd1.Parameters.Add(returnVal);

                    OracleParameter p1 = new OracleParameter("p_id_empresa", this.comboBoxEmpresa.Text.Split(' ')[0].Trim());
                    p1.Direction = ParameterDirection.Input;
                    cmd1.Parameters.Add(p1);

                    OracleParameter p2 = new OracleParameter("p_cod_tabela_orig", this.comboBoxTabelasOrigem.Text);
                    p2.Direction = ParameterDirection.Input;
                    cmd1.Parameters.Add(p2);

                    try
                    {
                        cmd1.ExecuteScalar();
                    }
                    catch (OracleException ex)
                    {
                        this.Cursor = Cursors.Default;
                        MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    if (returnVal.Value.ToString() == string.Empty)
                    {
                        OracleCommand cmd2 = new OracleCommand();

                        cmd2.Connection = Database.connection;
                        cmd2.CommandText = "gsf_tabela_orig_pkg.Inserir";
                        cmd2.CommandType = CommandType.StoredProcedure;

                        cmd2.Parameters.Add("p_id_empresa", this.comboBoxEmpresa.Text.Split(' ')[0].Trim());
                        cmd2.Parameters.Add("p_cod_tabela_orig", this.comboBoxTabelasOrigem.Text);
                        cmd2.Parameters.Add("p_ds_tabela_orig", this.textBoxDescricao.Text.Trim());

                        try
                        {
                            cmd2.ExecuteNonQuery();
                        }
                        catch (OracleException ex)
                        {
                            this.Cursor = Cursors.Default;
                            MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                    }
                    else
                    {
                        OracleCommand cmd2 = new OracleCommand();

                        cmd2.Connection = Database.connection;
                        cmd2.CommandText = "gsf_tabela_orig_pkg.Alterar";
                        cmd2.CommandType = CommandType.StoredProcedure;

                        cmd2.Parameters.Add("p_id_empresa", this.comboBoxEmpresa.Text.Split(' ')[0].Trim());
                        cmd2.Parameters.Add("p_cod_tabela_orig", this.comboBoxTabelasOrigem.Text);
                        cmd2.Parameters.Add("p_ds_tabela_orig", this.textBoxDescricao.Text.Trim());

                        try
                        {
                            cmd2.ExecuteNonQuery();
                        }
                        catch (OracleException ex)
                        {
                            this.Cursor = Cursors.Default;
                            System.Console.WriteLine("Exception: {0}", ex.ToString());
                            MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                    }

                    this.Cursor = Cursors.Default;

                    this.Close();
                }

            }
            else
            {
                this.Cursor = Cursors.WaitCursor;

                OracleCommand cmd = new OracleCommand();

                cmd.Connection = Database.connection;
                cmd.CommandText = "gsf_tabela_orig_pkg.Remover";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("p_id_empresa", this.comboBoxEmpresa.Text.Split(' ')[0].Trim());
                cmd.Parameters.Add("p_cod_tabela_orig", this.comboBoxTabelasOrigem.Text);
                OracleParameter pLog = new OracleParameter("p_id_log", null);
                pLog.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(pLog);

                try
                {
                    cmd.ExecuteNonQuery();

                    if (cmd.Parameters["p_id_log"].Value.ToString() != string.Empty)
                    {
                        OracleCommand cmdErro = new OracleCommand();

                        cmdErro.Connection = Database.connection;
                        cmdErro.CommandText = "gsf_log_pkg.Obtem_Texto_Log";
                        cmdErro.CommandType = CommandType.StoredProcedure;

                        OracleParameter returnVal = new OracleParameter("RETURN_VALUE", null);
                        returnVal.Direction = ParameterDirection.ReturnValue;
                        returnVal.Size = 2000;
                        cmdErro.Parameters.Add(returnVal);

                        OracleParameter p1 = new OracleParameter("p_id_log", OracleDbType.Int32);
                        p1.Direction = ParameterDirection.Input;
                        p1.Value = cmd.Parameters["p_id_log"].Value;
                        cmdErro.Parameters.Add(p1);

                        try
                        {
                            cmdErro.ExecuteNonQuery();

                            if (cmdErro.Parameters["RETURN_VALUE"].Value.ToString() != string.Empty)
                            {
                                this.Cursor = Cursors.Default;
                                string texto = cmdErro.Parameters["RETURN_VALUE"].Value.ToString();
                                MessageBox.Show("Ocorreu erro ao excluir:" + Environment.NewLine + Environment.NewLine + texto, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                return;
                            }
                        }
                        catch (OracleException ex)
                        {
                            this.Cursor = Cursors.Default;
                            MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                    }


                }
                catch (OracleException ex)
                {
                    this.Cursor = Cursors.Default;
                    MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                this.Cursor = Cursors.Default;

                this.Close();
            }
        }

        private void dataGridViewTabelasOrigem_CellContentClick(object sender, DataGridViewCellEventArgs e)

        {

        }

        private void listViewTabelasOrigem_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void comboBoxEmpresa_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void ResetFields()
        {
            foreach (Control ctrl in this.Controls)
            {
                if (ctrl is TextBox)
                {
                    TextBox tb = (TextBox)ctrl;
                    if (tb != null)
                    {
                        tb.Text = string.Empty;
                    }
                }
                else if (ctrl is ComboBox)
                {
                    ComboBox dd = (ComboBox)ctrl;
                    if (dd != null)
                    {
                        dd.Text = string.Empty;
                        dd.SelectedIndex = -1;
                    }
                }
            }
        }

        public void comboBoxEmpresa_DropDown(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            this.comboBoxEmpresa.Items.Clear();

            OracleCommand cmd = new OracleCommand();

            cmd.Connection = Database.connection;
            cmd.CommandText = "SELECT DISTINCT id \"Id. Empresa\", nome_empresa \"Nome da Empresa\" FROM gsf_empresa ORDER BY 1";
            cmd.CommandType = CommandType.Text;

            try
            { 
                OracleDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    this.comboBoxEmpresa.Items.Add(dr[0].ToString().PadRight(dr[0].ToString().Length) + " " + dr[1].ToString().PadRight(dr[0].ToString().Length));
                }

                dr.Close();
            }
            catch (OracleException ex)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            this.Cursor = Cursors.Default;
        }

        private void comboBoxDBLink_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        public void comboBoxDBLink_DropDown(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            this.comboBoxDBLink.Items.Clear();

            OracleCommand cmd = new OracleCommand();

            cmd.Connection = Database.connection;
            cmd.CommandText = "SELECT t.db_link FROM sys.user_db_links t";
            cmd.CommandType = CommandType.Text;

            try
            {
                OracleDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    this.comboBoxDBLink.Items.Add(dr[0].ToString().PadRight(dr[0].ToString().Length));
                }

                dr.Close();
            }
            catch (OracleException ex)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            this.Cursor = Cursors.Default;

        }

        private void FormTabelasOrigem_FormClosing(object sender, FormClosingEventArgs e)
        {
            ResetFields();
        }

        private void comboBoxTabelasOrigem_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        public void comboBoxTabelasOrigem_DropDown(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            this.comboBoxTabelasOrigem.Items.Clear();

            OracleCommand cmd = new OracleCommand();

            cmd.Connection = Database.connection;

            if (this.comboBoxDBLink.SelectedItem != null)
            {
                cmd.CommandText = "SELECT table_name FROM sys.user_tables@" + this.comboBoxDBLink.Text + " ORDER BY 1";
            }
            else
            {
                cmd.CommandText = "SELECT table_name FROM sys.user_tables ORDER BY 1";
            }

            try
            {
                cmd.CommandType = CommandType.Text;

                OracleDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    this.comboBoxTabelasOrigem.Items.Add(dr[0].ToString().PadRight(dr[0].ToString().Length));
                }

                dr.Close();
            }
            catch ( OracleException ex)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            this.Cursor = Cursors.Default;
        }

        private void FormTabelasOrigem_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }

        }

        private void comboBoxDBLink_SelectedValueChanged(object sender, EventArgs e)
        {
            if (this.comboBoxEmpresa.Text != string.Empty && this.comboBoxTabelasOrigem.Text != string.Empty && this.textBoxDescricao.Text.Trim() != string.Empty)
            {
                this.buttonOK.Enabled = true;
            }
            else
            {
                this.buttonOK.Enabled = false;
            }
        }
    }
}

