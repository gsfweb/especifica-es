﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
//using System.Data.OleDb;
using Oracle.DataAccess.Client;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MapWinApp
{
    public partial class FormCriteriosSelecao : Form
    {
        public FormCriteriosSelecao()
        {
            InitializeComponent();
        }

        private void FormCriteriosSelecao_Load(object sender, EventArgs e)
        {

        }

        private void buttonIncluirRemover_Click(object sender, EventArgs e)
        {
            if (this.buttonIncluirRemover.Text.Contains("Incluir"))
            {
                if (this.textBoxValorPesquisa.Text != string.Empty &&
                    this.comboBoxAtributosOrigem.Text != string.Empty &&
                    this.comboBoxOperadorRelacional.Text != string.Empty &&
                    this.textBoxValorPesquisa.Text != string.Empty &&
                    this.comboBoxOperadorLogico.Text != string.Empty)
                {
                    this.Cursor = Cursors.WaitCursor;

                    FormMapeamento pai = (FormMapeamento)this.Owner;

                    OracleParameter p1 = new OracleParameter("p_id_map", OracleDbType.Int32);
                    p1.Direction = ParameterDirection.Input;

                    OracleParameter p2 = new OracleParameter("p_cod_atr_orig", OracleDbType.Varchar2);
                    p2.Direction = ParameterDirection.Input;

                    OracleParameter p3 = new OracleParameter("p_oper_relacional", OracleDbType.Varchar2);
                    p3.Direction = ParameterDirection.Input;

                    OracleParameter p4 = new OracleParameter("p_condicao", OracleDbType.Varchar2);
                    p4.Direction = ParameterDirection.Input;

                    OracleParameter p5 = new OracleParameter("p_oper_logico", OracleDbType.Varchar2);
                    p5.Direction = ParameterDirection.Input;

                    OracleParameter p6 = new OracleParameter("p_id_order", OracleDbType.Int32);
                    p6.Direction = ParameterDirection.Output;

                    OracleCommand cmd = new OracleCommand();

                    cmd.Connection = Database.connection;
                    cmd.CommandText = "GSF_CRIT_SELECAO_MAP_PKG.INSERIR";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(p1);
                    cmd.Parameters.Add(p2);
                    cmd.Parameters.Add(p3);
                    cmd.Parameters.Add(p4);
                    cmd.Parameters.Add(p5);
                    cmd.Parameters.Add(p6);

                    cmd.Parameters["p_id_map"].Value = pai.textBoxIdMapeamento.Text;
                    cmd.Parameters["p_cod_atr_orig"].Value = this.comboBoxAtributosOrigem.Text;
                    cmd.Parameters["p_oper_relacional"].Value = this.comboBoxOperadorRelacional.Text;
                    cmd.Parameters["p_condicao"].Value = this.textBoxValorPesquisa.Text;
                    cmd.Parameters["p_oper_logico"].Value = this.comboBoxOperadorLogico.Text;

                    try
                    {
                        cmd.ExecuteNonQuery();

                        try
                        {
                            pai.listViewCriteriosSelecaoOrigem.Items.Add(this.comboBoxAtributosOrigem.Text);
                            pai.listViewCriteriosSelecaoOrigem.Items[pai.listViewCriteriosSelecaoOrigem.Items.Count - 1].Tag = cmd.Parameters["p_id_order"].Value;
                            pai.listViewCriteriosSelecaoOrigem.Items[pai.listViewCriteriosSelecaoOrigem.Items.Count - 1].SubItems.Add(this.comboBoxOperadorRelacional.Text);
                            pai.listViewCriteriosSelecaoOrigem.Items[pai.listViewCriteriosSelecaoOrigem.Items.Count - 1].SubItems.Add(this.textBoxValorPesquisa.Text);
                            pai.listViewCriteriosSelecaoOrigem.Items[pai.listViewCriteriosSelecaoOrigem.Items.Count - 1].SubItems.Add(this.comboBoxOperadorLogico.Text);

                            this.comboBoxAtributosOrigem.SelectedIndex = -1;
                            this.comboBoxOperadorRelacional.SelectedIndex = -1;
                            this.textBoxValorPesquisa.Text = string.Empty;
                            this.comboBoxOperadorLogico.SelectedIndex = -1;

                            this.ActiveControl = this.comboBoxAtributosOrigem;

                            this.Cursor = Cursors.Default;
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    catch (OracleException ex)
                    {
                        this.Cursor = Cursors.Default;
                        MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else
            {
                if (this.textBoxValorPesquisa.Text != string.Empty &&
                    this.comboBoxAtributosOrigem.Text != string.Empty &&
                    this.comboBoxOperadorRelacional.Text != string.Empty &&
                    this.textBoxValorPesquisa.Text != string.Empty)
                {
                    this.Cursor = Cursors.WaitCursor;

                    FormMapeamento pai = (FormMapeamento)this.Owner;

                    OracleParameter p1 = new OracleParameter("p_id_map", OracleDbType.Int32);
                    p1.Direction = ParameterDirection.Input;

                    OracleParameter p2 = new OracleParameter("p_id_ordem", OracleDbType.Int32);
                    p2.Direction = ParameterDirection.Input;

                    OracleCommand cmd = new OracleCommand();

                    cmd.Connection = Database.connection;
                    cmd.CommandText = "GSF_CRIT_SELECAO_MAP_PKG.Remover";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(p1);
                    cmd.Parameters.Add(p2);

                    cmd.Parameters["p_id_map"].Value = pai.textBoxIdMapeamento.Text;
                    cmd.Parameters["p_id_ordem"].Value = pai.listViewCriteriosSelecaoOrigem.SelectedItems[0].Tag;

                    try
                    {
                        cmd.ExecuteNonQuery();

                        try
                        {
                            pai.listViewCriteriosSelecaoOrigem.SelectedItems[0].Remove();
                            this.Cursor = Cursors.Default;
                            this.Close();
                        }
                        catch (Exception ex)
                        {
                            this.Cursor = Cursors.Default;
                            MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    catch (OracleException ex)
                    {
                        this.Cursor = Cursors.Default;
                        MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void VerificaDados(object sender, EventArgs e)
        {
            if (this.comboBoxAtributosOrigem.Text != string.Empty && 
                this.comboBoxOperadorRelacional.Text != string.Empty &&
                this.textBoxValorPesquisa.Text.Trim() != string.Empty &&
                this.comboBoxOperadorLogico.Text != string.Empty)
            {
                this.buttonIncluirRemover.Enabled = true;
            }
            else
            {
                this.buttonIncluirRemover.Enabled = false;
            }
        }

        private void FormCriteriosSelecao_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }

        }

        private void comboBoxAtributosOrigem_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBoxOperadorLogico_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
