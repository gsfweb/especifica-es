﻿namespace MapWinApp
{
    partial class FormConexao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormConexao));
            this.comboBoxTNSNames = new System.Windows.Forms.ComboBox();
            this.comboBoxOracleHomes = new System.Windows.Forms.ComboBox();
            this.labelOracleHome = new System.Windows.Forms.Label();
            this.labelTNSName = new System.Windows.Forms.Label();
            this.textBoxUserName = new System.Windows.Forms.TextBox();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.labelUserName = new System.Windows.Forms.Label();
            this.labelPassword = new System.Windows.Forms.Label();
            this.buttonOK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // comboBoxTNSNames
            // 
            this.comboBoxTNSNames.FormattingEnabled = true;
            this.comboBoxTNSNames.Location = new System.Drawing.Point(12, 77);
            this.comboBoxTNSNames.Name = "comboBoxTNSNames";
            this.comboBoxTNSNames.Size = new System.Drawing.Size(283, 21);
            this.comboBoxTNSNames.TabIndex = 3;
            this.comboBoxTNSNames.SelectedIndexChanged += new System.EventHandler(this.comboBoxTNSNames_SelectedIndexChanged);
            // 
            // comboBoxOracleHomes
            // 
            this.comboBoxOracleHomes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxOracleHomes.FormattingEnabled = true;
            this.comboBoxOracleHomes.Location = new System.Drawing.Point(12, 25);
            this.comboBoxOracleHomes.Name = "comboBoxOracleHomes";
            this.comboBoxOracleHomes.Size = new System.Drawing.Size(283, 21);
            this.comboBoxOracleHomes.TabIndex = 1;
            this.comboBoxOracleHomes.SelectedIndexChanged += new System.EventHandler(this.comboBoxOracleHomes_SelectedIndexChanged);
            // 
            // labelOracleHome
            // 
            this.labelOracleHome.AutoSize = true;
            this.labelOracleHome.Location = new System.Drawing.Point(12, 9);
            this.labelOracleHome.Name = "labelOracleHome";
            this.labelOracleHome.Size = new System.Drawing.Size(72, 13);
            this.labelOracleHome.TabIndex = 0;
            this.labelOracleHome.Text = "Oracle Home:";
            this.labelOracleHome.Click += new System.EventHandler(this.labelOracleHomes_Click);
            // 
            // labelTNSName
            // 
            this.labelTNSName.AutoSize = true;
            this.labelTNSName.Location = new System.Drawing.Point(12, 61);
            this.labelTNSName.Name = "labelTNSName";
            this.labelTNSName.Size = new System.Drawing.Size(63, 13);
            this.labelTNSName.TabIndex = 2;
            this.labelTNSName.Text = "TNS Name:";
            // 
            // textBoxUserName
            // 
            this.textBoxUserName.Location = new System.Drawing.Point(12, 126);
            this.textBoxUserName.Name = "textBoxUserName";
            this.textBoxUserName.Size = new System.Drawing.Size(167, 20);
            this.textBoxUserName.TabIndex = 5;
            this.textBoxUserName.TextChanged += new System.EventHandler(this.textBoxUserName_TextChanged);
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Location = new System.Drawing.Point(12, 174);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.PasswordChar = '*';
            this.textBoxPassword.Size = new System.Drawing.Size(167, 20);
            this.textBoxPassword.TabIndex = 7;
            this.textBoxPassword.TextChanged += new System.EventHandler(this.textBoxPassword_TextChanged);
            // 
            // labelUserName
            // 
            this.labelUserName.AutoSize = true;
            this.labelUserName.Location = new System.Drawing.Point(13, 111);
            this.labelUserName.Name = "labelUserName";
            this.labelUserName.Size = new System.Drawing.Size(46, 13);
            this.labelUserName.TabIndex = 4;
            this.labelUserName.Text = "Usuário:";
            this.labelUserName.Click += new System.EventHandler(this.labelUserName_Click);
            // 
            // labelPassword
            // 
            this.labelPassword.AutoSize = true;
            this.labelPassword.Location = new System.Drawing.Point(12, 158);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(41, 13);
            this.labelPassword.TabIndex = 6;
            this.labelPassword.Text = "Senha:";
            this.labelPassword.Click += new System.EventHandler(this.labelPassword_Click);
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(220, 215);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 8;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // FormConexao
            // 
            this.AcceptButton = this.buttonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(306, 250);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.labelPassword);
            this.Controls.Add(this.labelUserName);
            this.Controls.Add(this.textBoxPassword);
            this.Controls.Add(this.textBoxUserName);
            this.Controls.Add(this.labelTNSName);
            this.Controls.Add(this.labelOracleHome);
            this.Controls.Add(this.comboBoxOracleHomes);
            this.Controls.Add(this.comboBoxTNSNames);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormConexao";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Conexão";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormConexao_FormClosed);
            this.Load += new System.EventHandler(this.FormConexao_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormConexao_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxTNSNames;
        private System.Windows.Forms.ComboBox comboBoxOracleHomes;
        private System.Windows.Forms.Label labelOracleHome;
        private System.Windows.Forms.Label labelTNSName;
        private System.Windows.Forms.TextBox textBoxUserName;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.Label labelUserName;
        private System.Windows.Forms.Label labelPassword;
        private System.Windows.Forms.Button buttonOK;
    }
}