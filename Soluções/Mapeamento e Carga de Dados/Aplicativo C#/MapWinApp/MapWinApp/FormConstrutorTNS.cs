﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MapWinApp
{
    public partial class FormConstrutorTNS : Form
    {
        private string TNSName = "(DESCRIPTION = (ADDRESS = (PROTOCOL = ?)(HOST = ?)(PORT = ?)) (CONNECT_DATA = (SERVER = DEDICATED) (SERVICE_NAME = ?)))";

        public FormConstrutorTNS()
        {
            InitializeComponent();
        }


        public void FormConstrutorTNS_Load(object sender, EventArgs e)
        {
            this.labelKeyPipePort.Tag = this.labelKeyPipePort.Location.X + ":" + this.labelKeyPipePort.Location.Y;
            this.textBoxKeyPipePort.Tag = this.textBoxKeyPipePort.Location.X + ":" + this.textBoxKeyPipePort.Location.Y;
        }

        private void textBoxNomeServico_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxServidor_TextChanged(object sender, EventArgs e)
        {
            if (this.textBoxHostServer.Text.Trim() != string.Empty && this.textBoxKeyPipePort.Text.Trim() != string.Empty && this.textBoxServiceName.Text.Trim() != string.Empty)
            {
                this.buttonOK.Enabled = true;
            }
            else
            {
                this.buttonOK.Enabled = false;
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            FormConexao pai = (FormConexao)this.Owner;

            pai.textBoxTNSName.Text = TNSName;

            if (this.comboBoxProtocolo.SelectedIndex == 0)
            {
                //IPC
                //PROTOCOL: Specify ipc as the value.
                pai.textBoxTNSName.Text = pai.textBoxTNSName.Text.Replace("(PROTOCOL = ?)", "(PROTOCOL = ipc)");
                pai.textBoxTNSName.Text = pai.textBoxTNSName.Text.Replace("(HOST = ?)", string.Empty);
                pai.textBoxTNSName.Text = pai.textBoxTNSName.Text.Replace("(PORT = ?)", "(KEY = " + this.textBoxKeyPipePort.Text.Trim() + ")");
            }
            else if (this.comboBoxProtocolo.SelectedIndex == 1)
            {
                //Named Pipes
                //PROTOCOL: Specify nmp as the value.
                pai.textBoxTNSName.Text = pai.textBoxTNSName.Text.Replace("(PROTOCOL = ?)", "(PROTOCOL = nmp)");
                pai.textBoxTNSName.Text = pai.textBoxTNSName.Text.Replace("(HOST = ?)", "(SERVER = " + this.textBoxHostServer.Text.Trim() + ")");
                pai.textBoxTNSName.Text = pai.textBoxTNSName.Text.Replace("(PORT = ?)", "(PIPE = " + this.textBoxKeyPipePort.Text.Trim() + ")");
            }
            else if (this.comboBoxProtocolo.SelectedIndex == 2)
            {
                //SDP
                //PROTOCOL: Specify sdp as the value.
                pai.textBoxTNSName.Text = pai.textBoxTNSName.Text.Replace("(PROTOCOL = ?)", "(PROTOCOL = sdp)");
                pai.textBoxTNSName.Text = pai.textBoxTNSName.Text.Replace("(HOST = ?)", "(HOST = " + this.textBoxHostServer.Text.Trim() + ")");
                pai.textBoxTNSName.Text = pai.textBoxTNSName.Text.Replace("(PORT = ?)", "(PORT = " + this.textBoxKeyPipePort.Text.Trim() + ")");
            }

            else if (this.comboBoxProtocolo.SelectedIndex == -1 || this.comboBoxProtocolo.SelectedIndex == 3)
            {
                //TCP / IP
                //PROTOCOL: Specify tcp as the value.
                pai.textBoxTNSName.Text = pai.textBoxTNSName.Text.Replace("(PROTOCOL = ?)", "(PROTOCOL = tcp)");
                pai.textBoxTNSName.Text = pai.textBoxTNSName.Text.Replace("(HOST = ?)", "(HOST = " + this.textBoxHostServer.Text.Trim() + ")");
                pai.textBoxTNSName.Text = pai.textBoxTNSName.Text.Replace("(PORT = ?)", "(PORT = " + this.textBoxKeyPipePort.Text.Trim() + ")");
            }
            else if (this.comboBoxProtocolo.SelectedIndex == 4)
            {
                //TCP / IP with SSL
                //PROTOCOL: Specify tcps as the value.
                pai.textBoxTNSName.Text = pai.textBoxTNSName.Text.Replace("(PROTOCOL = ?)", "(PROTOCOL = tcps)");
                pai.textBoxTNSName.Text = pai.textBoxTNSName.Text.Replace("(HOST = ?)", "(HOST = " + this.textBoxHostServer.Text.Trim() + ")");
                pai.textBoxTNSName.Text = pai.textBoxTNSName.Text.Replace("(PORT = ?)", "(PORT = " + this.textBoxKeyPipePort.Text.Trim() + ")");
            }

            pai.textBoxTNSName.Text = pai.textBoxTNSName.Text.Replace("SERVICE_NAME = ?", "SERVICE_NAME = " + this.textBoxServiceName.Text.Trim());

            this.Close();
        }

        private void textBoxPorta_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void comboBoxProtocolo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.comboBoxProtocolo.SelectedIndex == 0)
            {
                //IPC
                //PROTOCOL: Specify ipc as the value.
                //KEY: Specify a unique name for the service. Oracle recommends using the service name or the Oracle System Identifier (SID)of the service.
                //Example: (PROTOCOL = ipc)(KEY = sales)

                this.labelHostServer.Visible = false;
                this.textBoxHostServer.Visible = false;

                this.labelKeyPipePort.Location = this.labelHostServer.Location;
                this.labelKeyPipePort.Text = "Key:";
                this.textBoxKeyPipePort.Location = this.textBoxHostServer.Location;
            }
            else if (this.comboBoxProtocolo.SelectedIndex == 1)
            {

                //Named Pipes
                //PROTOCOL: Specify nmp as the value.
                //SERVER: Specify the name of the Oracle server computer.
                //PIPE: Specify the pipe name you used to connect to the database server(the same PIPE keyword you specified on server with Named Pipes).This name can be any arbitrary name.
                //Example:(PROTOCOL = nmp)(SERVER = sales)(PIPE = dbpipe0)

                this.labelHostServer.Visible = true;
                this.labelHostServer.Text = "Server:";
                this.textBoxHostServer.Visible = true;

                this.labelKeyPipePort.Location = new System.Drawing.Point(int.Parse(this.labelKeyPipePort.Tag.ToString().Split(':')[0]), int.Parse(this.labelKeyPipePort.Tag.ToString().Split(':')[1])) ;
                this.textBoxKeyPipePort.Location = new System.Drawing.Point(int.Parse(this.textBoxKeyPipePort.Tag.ToString().Split(':')[0]), int.Parse(this.textBoxKeyPipePort.Tag.ToString().Split(':')[1]));
                this.labelKeyPipePort.Text = "Pipe:";
            }


            else if (this.comboBoxProtocolo.SelectedIndex == 2)
            {

                //SDP
                //PROTOCOL: Specify sdp as the value.
                //HOST: Specify the host name or IP address of the computer.
                //PORT: Specify the listening port number.
                //Example: (PROTOCOL = sdp)(HOST = sales - server)(PORT = 1521)
                //         (PROTOCOL = sdp)(HOST = 192.0.2.204)(PORT = 1521)
                //See Also: "Recommended Port Numbers"

                this.labelHostServer.Visible = true;
                this.labelHostServer.Text = "Host:";
                this.textBoxHostServer.Visible = true;

                this.labelKeyPipePort.Location = new System.Drawing.Point(int.Parse(this.labelKeyPipePort.Tag.ToString().Split(':')[0]), int.Parse(this.labelKeyPipePort.Tag.ToString().Split(':')[1]));
                this.textBoxKeyPipePort.Location = new System.Drawing.Point(int.Parse(this.textBoxKeyPipePort.Tag.ToString().Split(':')[0]), int.Parse(this.textBoxKeyPipePort.Tag.ToString().Split(':')[1]));
                this.labelKeyPipePort.Text = "Port:";
            }

            else if (this.comboBoxProtocolo.SelectedIndex == 3)
            {

                //TCP / IP
                //PROTOCOL: Specify tcp as the value.
                //HOST: Specify the host name or IP address of the computer.
                //PORT: Specify the listening port number.
                //Example: (PROTOCOL = tcp)(HOST = sales - server)(PORT = 1521)
                //         (PROTOCOL = tcp)(HOST = 192.0.2.204)(PORT = 1521)
                //See Also: "Recommended Port Numbers"

                this.labelHostServer.Visible = true;
                this.labelHostServer.Text = "Host:";
                this.textBoxHostServer.Visible = true;

                this.labelKeyPipePort.Location = new System.Drawing.Point(int.Parse(this.labelKeyPipePort.Tag.ToString().Split(':')[0]), int.Parse(this.labelKeyPipePort.Tag.ToString().Split(':')[1]));
                this.textBoxKeyPipePort.Location = new System.Drawing.Point(int.Parse(this.textBoxKeyPipePort.Tag.ToString().Split(':')[0]), int.Parse(this.textBoxKeyPipePort.Tag.ToString().Split(':')[1]));
                this.labelKeyPipePort.Text = "Port:";
            }

            else if (this.comboBoxProtocolo.SelectedIndex == 4)
            {

                //TCP / IP with SSL
                //PROTOCOL: Specify tcps as the value.
                //HOST: Specify the host name or IP address of the computer.
                //PORT: Specify the listening port number.
                //Example: (PROTOCOL = tcps)(HOST = sales - server)(PORT = 2484)
                //         (PROTOCOL = tcps)(HOST = 192.0.2.204)(PORT = 2484)

                this.labelHostServer.Visible = true;
                this.labelHostServer.Text = "Host:";
                this.textBoxHostServer.Visible = true;

                this.labelKeyPipePort.Location = new System.Drawing.Point(int.Parse(this.labelKeyPipePort.Tag.ToString().Split(':')[0]), int.Parse(this.labelKeyPipePort.Tag.ToString().Split(':')[1]));
                this.textBoxKeyPipePort.Location = new System.Drawing.Point(int.Parse(this.textBoxKeyPipePort.Tag.ToString().Split(':')[0]), int.Parse(this.textBoxKeyPipePort.Tag.ToString().Split(':')[1]));
                this.labelKeyPipePort.Text = "Port:";
            }
        }
    }
}
