CREATE OR REPLACE PACKAGE GSF_CRIT_SELECAO_MAP_PKG IS

PROCEDURE INSERIR( p_id_map          gsf_crit_selecao_map_tab.id_map%TYPE
                 , p_cod_atr_orig    gsf_crit_selecao_map_tab.cod_atr_orig%TYPE
                 , p_oper_relacional gsf_crit_selecao_map_tab.oper_relacional%TYPE
                 , p_condicao        gsf_crit_selecao_map_tab.condicao%TYPE
                 , p_oper_logico     gsf_crit_selecao_map_tab.oper_logico%TYPE );
                 
PROCEDURE ALTERAR( p_id_map          gsf_crit_selecao_map_tab.id_map%TYPE
                 , p_id_ordem        gsf_crit_selecao_map_tab.id_ordem%TYPE
                 , p_cod_atr_orig    gsf_crit_selecao_map_tab.cod_atr_orig%TYPE
                 , p_oper_relacional gsf_crit_selecao_map_tab.oper_relacional%TYPE
                 , p_condicao        gsf_crit_selecao_map_tab.condicao%TYPE
                 , p_oper_logico     gsf_crit_selecao_map_tab.oper_logico%TYPE ); 
                 
PROCEDURE REMOVER( p_id_map          gsf_crit_selecao_map_tab.id_map%TYPE
                 , p_id_ordem        gsf_crit_selecao_map_tab.id_ordem%TYPE );                                 
                 
END GSF_CRIT_SELECAO_MAP_PKG;
/
CREATE OR REPLACE PACKAGE BODY GSF_CRIT_SELECAO_MAP_PKG IS

v_sql_dynamic varchar2(2000);

PROCEDURE INSERIR( p_id_map          gsf_crit_selecao_map_tab.id_map%TYPE
                 , p_cod_atr_orig    gsf_crit_selecao_map_tab.cod_atr_orig%TYPE
                 , p_oper_relacional gsf_crit_selecao_map_tab.oper_relacional%TYPE
                 , p_condicao        gsf_crit_selecao_map_tab.condicao%TYPE 
                 , p_oper_logico     gsf_crit_selecao_map_tab.oper_logico%TYPE ) is
                 
       v_id_map          gsf_crit_selecao_map_tab.id_map%TYPE;
       v_cod_atr_orig    gsf_crit_selecao_map_tab.cod_atr_orig%TYPE;
       v_oper_relacional gsf_crit_selecao_map_tab.oper_relacional%TYPE;
       v_condicao        gsf_crit_selecao_map_tab.condicao%TYPE;
       v_oper_logico     gsf_crit_selecao_map_tab.oper_logico%TYPE;
       v_id_objeto       gsf_crit_selecao_map_tab.id_objeto%TYPE;
       v_id_ordem        gsf_crit_selecao_map_tab.id_ordem%TYPE;
       
       begin 
         
         v_id_map          := p_id_map;
         v_cod_atr_orig    := p_cod_atr_orig;
         v_oper_relacional := p_oper_relacional;
         v_condicao        := p_condicao;
         v_oper_logico     := p_oper_logico;
         
         --pega �ndice 
         SELECT COUNT(ID_MAP)+1
           INTO v_id_ordem
           FROM GSF_CRIT_SELECAO_MAP_TAB   
          WHERE ID_MAP = v_id_map;
         
         v_sql_dynamic := 'INSERT INTO GSF_CRIT_SELECAO_MAP_TAB( ID_MAP
                                                              , COD_ATR_ORIG
                                                              , ID_ORDEM
                                                              , OPER_RELACIONAL
                                                              , CONDICAO
                                                              , OPER_LOGICO
                                                              , VERSAO_LINHA ) 
                                                       VALUES ( :id_map
                                                              , :cod_atr_orig
                                                              , :id_ordem
                                                              , :oper_relacional
                                                              , :condicao
                                                              , :oper_logico
                                                              , sysdate )';
         
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_id_map
             , v_cod_atr_orig
             , v_id_ordem
             , v_oper_relacional  
             , v_condicao
             , v_oper_logico;                                                                
                 
         SELECT ROWID 
           INTO v_id_objeto 
           FROM GSF_CRIT_SELECAO_MAP_TAB 
          WHERE ID_MAP   = v_id_map
            AND ID_ORDEM = v_id_ordem;
            
         v_sql_dynamic := 'UPDATE GSF_CRIT_SELECAO_MAP_TAB
                              SET ID_OBJETO = :v_id_objeto
                            WHERE ID_MAP   = :id_map
                              AND ID_ORDEM = :id_ordem';
         
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_id_objeto
             , v_id_map
             , v_id_ordem;
             
         COMMIT;
         
         EXCEPTION
           WHEN others THEN
             ROLLBACK; 
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'GSF_CRIT_SELECAO_MAP_PKG.INSERIR');
             dbms_output.put_line(SQLERRM);    
         
END INSERIR;

PROCEDURE ALTERAR( p_id_map          gsf_crit_selecao_map_tab.id_map%TYPE
                 , p_id_ordem        gsf_crit_selecao_map_tab.id_ordem%TYPE
                 , p_cod_atr_orig    gsf_crit_selecao_map_tab.cod_atr_orig%TYPE
                 , p_oper_relacional gsf_crit_selecao_map_tab.oper_relacional%TYPE
                 , p_condicao        gsf_crit_selecao_map_tab.condicao%TYPE
                 , p_oper_logico     gsf_crit_selecao_map_tab.oper_logico%TYPE ) IS
                 
       v_id_map          gsf_crit_selecao_map_tab.id_map%TYPE;
       v_id_ordem        gsf_crit_selecao_map_tab.id_ordem%TYPE;
       v_cod_atr_orig    gsf_crit_selecao_map_tab.cod_atr_orig%TYPE;
       v_oper_relacional gsf_crit_selecao_map_tab.oper_relacional%TYPE;
       v_condicao        gsf_crit_selecao_map_tab.condicao%TYPE;
       v_oper_logico     gsf_crit_selecao_map_tab.oper_logico%TYPE;
       
       BEGIN
         
         v_id_map          := p_id_map;
         v_id_ordem        := p_id_ordem;
         v_cod_atr_orig    := p_cod_atr_orig;
         v_oper_relacional := p_oper_relacional;
         v_condicao        := p_condicao;
         v_oper_logico     := p_oper_logico;
         
         v_sql_dynamic := 'UPDATE GSF_CRIT_SELECAO_MAP_TAB
                              SET COD_ATR_ORIG    = :cod_atr_orig
                                , OPER_RELACIONAL = :oper_relacional
                                , CONDICAO        = :condicao
                                , OPER_LOGICO     = :oper_logico
                            WHERE ID_MAP   = :id_map
                              AND ID_ORDEM = :id_ordem';
                           
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_cod_atr_orig
             , v_oper_relacional
             , v_condicao
             , v_oper_logico
             , v_id_map
             , v_id_ordem;
             
         COMMIT;     
                                   
         EXCEPTION
           WHEN others THEN
             ROLLBACK; 
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'GSF_CRIT_SELECAO_MAP_PKG.ALTERAR');
             dbms_output.put_line(SQLERRM);    

END ALTERAR;


PROCEDURE REMOVER( p_id_map          gsf_crit_selecao_map_tab.id_map%TYPE
                 , p_id_ordem        gsf_crit_selecao_map_tab.id_ordem%TYPE ) IS
                 
       v_id_map          gsf_crit_selecao_map_tab.id_map%TYPE;
       v_id_ordem        gsf_crit_selecao_map_tab.id_ordem%TYPE;
       
       BEGIN
         
         v_id_map   := p_id_map;
         v_id_ordem := p_id_ordem;
         
         v_sql_dynamic := 'DELETE 
                             FROM GSF_CRIT_SELECAO_MAP_TAB
                            WHERE ID_MAP   = :id_map
                              AND ID_ORDEM = :id_ordem';
                              
         
         EXECUTE IMMEDIATE v_sql_dynamic 
         USING v_id_map
             , v_id_ordem;
             
         COMMIT;
         
         EXCEPTION
           WHEN others THEN
             ROLLBACK; 
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'GSF_CRIT_SELECAO_MAP_PKG.REMOVER');
             dbms_output.put_line(SQLERRM);                


END REMOVER;

END GSF_CRIT_SELECAO_MAP_PKG;
/
