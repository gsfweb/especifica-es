create or replace view gsf_tabela_dest as
select COD_TABELA_DEST
     , DESC_TABELA_DEST
     , VERSAO_LINHA
     , ID_OBJETO
 from GSF_TABELA_DEST_TAB
WITH READ ONLY;
