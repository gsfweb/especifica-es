create or replace view gsf_grupo_tabela as
select COD_MODULO
     , DESC_MODULO
     , VERSAO_LINHA
     , ID_OBJETO
 from gsf_grupo_tabela_tab
WITH READ ONLY;
